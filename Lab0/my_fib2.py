# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 12:30:16 2022

@author: Barrett Osborne
"""
"Fibonacci Function"
def fib(x):
    
    n0 = 0
    n1 = 1
    i = 0
    
    if x == 0:
        n = n0
        
    elif x == 1:
        n = n1
        
    else:
    
        while i < x - 1:
            n = n0 + n1
            n0 = n1
            n1 = n 
            i = i + 1
    
    return n    

" User Interface"

if __name__ == '__main__':
    
    idx = (input("Choose an index: " ))
    int_check = str(idx.isdigit())

    while int_check == "False":
        print(f"index must be a number!")
        idx = (input("Choose an index: " ))
        int_check = str(idx.isdigit())
        
    fibo = fib(int(idx))
    print(f"Fibonacci number at index {idx} is {fibo}.")
        
    end_game = str()

    while end_game != "q":
        end_game = input("Press q to quit or e to choose a new index: ")
    
        if end_game == "e":
            idx = (input("Choose an index: " ))
            int_check = str(idx.isdigit())

            while int_check == "False":
                print(f"index must be a number!")
                idx = (input("Choose an index: " ))
                int_check = str(idx.isdigit())
        
            fibo = fib(int(idx))
            print(f"Fibonacci number at index {idx} is {fibo}.")
    
        





    
