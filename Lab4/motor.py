'''!@file motor.py
    @brief          Creates motor object
    @details        Creates a motor object with a function "set_duty" which sets the duty cycle
                    for the PWM that is applied to the motor.
    @author         O'Connell, Joseph
    @author         Osborne, Barrett
    @date           February 3, 2022

'''
from pyb import Pin, Timer

class Motor:
    '''!@brief A motor class for one channel of the DRV8847.
        @details Objects of this class can be used to apply PWM to a given
        DC motor.
    '''
    
    def __init__ (self, PWM_tim, IN1_pin, IN2_pin, ch1, ch2):
        '''!@brief      Initializes and returns an object associated with a DC Motor.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
            @param PWM_tim  Timer object
            @param IN1_pin  Initializastion pin 1
            @param IN2_pin  Initializastion pin 1
            @param ch1      Channel 1
            @param ch2      Channel 2
        '''
        
        ## @brief PWM channel 1
        #
        self.PWM_tim_ch1 = PWM_tim.channel(ch1 , Timer.PWM , pin = IN1_pin)
        
        ## @brief PWM channel 2
        #
        self.PWM_tim_ch2 = PWM_tim.channel(ch2 , Timer.PWM , pin = IN2_pin)
        
        pass
    
    def set_duty (self, duty):
        '''!@brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param duty A signed number holding the duty
                        cycle of the PWM signal sent to the motor
        '''
        
        if duty > 0:
            
            self.PWM_tim_ch1.pulse_width_percent(100)
            #in1 high
            
            self.PWM_tim_ch2.pulse_width_percent(100 - duty)
            #in2 modded
            
        elif duty <= 0:
        
            self.PWM_tim_ch1.pulse_width_percent(100 + duty)
            #in1 modded
            
            self.PWM_tim_ch2.pulse_width_percent(100)
            #in2 high
            
        
        pass
    
    
if __name__ == '__main__':
    
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    # Create a timer object to use for motor control
        
    PWM_tim = Timer(3, freq = 20_000)
    
    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
    
    motor_1 = Motor(PWM_tim, Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    motor_2 = Motor(PWM_tim, Pin.cpu.B0, Pin.cpu.B1, 3, 4)
    
    # Enable the motor driver
    
    nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
    nSLEEP.high()
    
    # Set the duty cycle of the first motor to 40 percent
    
    motor_1.set_duty(0)
    motor_2.set_duty(0)
