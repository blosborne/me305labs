'''!    @file closedLoop.py
        @brief                                  Acts as a proportional controller for a given input and reference value
        @details                                Takes in a value and a reference value. Computes the difference between the two.
                                                This difference is multiplied by a user supplied gain.
                                                
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   February 10, 2022
'''

class ClosedLoop:
    '''!@brief                                  Acts as a proportional controller for a given input and reference value
        @details                                Takes in a value and a reference value. Computes the difference between the two.
                                                This difference is multiplied by a user supplied gain.
    '''
    def __init__(self, reference_value):
        '''!@brief                              Defines reference value for propportional controller
        '''
        
        self.reference = reference_value
        
        pass
 
    def setKp(self, kProp):
        '''!@brief                              Defines proportional controller constant
        '''
        
        self.gain = kProp
        
        pass
       
    def propCont(self, measured_value):
        '''!@brief                              Acts as a proportional controller for a given input and reference value
            @details                                Takes in a value and a reference value. Computes the difference between the two.
                                                This difference is multiplied by a user supplied gain.
        '''
        
        self.output = self.gain*(self.reference - measured_value)
        
        return self.output
    

