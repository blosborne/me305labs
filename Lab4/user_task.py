'''!    @file user_task.py
        @brief                                  Generates and manages a user interface for the encoder
        @details                                The user interface has the following functions;

                                                         (z or Z)   Zero the encoder                       
                                                         (p or P)   Print encoder position                 
                                                         (d or D)   Print encoder delta                    
                                                         (v or V)   Print encoder velocity                 
                                                         (e or E)   Enable motors                          
                                                         (y or Y)   Enter set point for motor 1            
                                                         (k or K)   Enter gain for motor 1                 
                                                         (w or W)   Enable or disable closed Loop control  
                                                         (r or R)   Perform step response on motor 1       
                                                         (m or M)   Enter duty cycle for motor 1           
                                                         (n or N)   Enter duty cycle for motor 2           
                                                         (c or C)   Clear a fault message                  
                                                         (g or G)   Collect data for 30 seconds            
                                                         (t or T)   Begin testing interface                
                                                         (s or S)   End data collection                    
                                                         (h or H)   Display this help message 
                                                                          
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   January 27, 2022
'''

import time , pyb , array

def taskUserFcn (taskName , period , zFlag, pFlag, dFlag, gFlag, sFlag, eFlag, nFlag, mFlag, tFlag, cFlag, position, delta, duty1, duty2, vel, wFlag, rFlag, ref_val, Kp):
    '''!@brief          Generates and manages a user interface for the encoder
        @details        The user interface has the following functions;

                                                         (z or Z)   Zero the encoder                       
                                                         (p or P)   Print encoder position                 
                                                         (d or D)   Print encoder delta                    
                                                         (v or V)   Print encoder velocity                 
                                                         (e or E)   Enable motors                          
                                                         (y or Y)   Enter set point for motor 1            
                                                         (k or K)   Enter gain for motor 1                 
                                                         (w or W)   Enable or disable closed Loop control  
                                                         (r or R)   Perform step response on motor 1       
                                                         (m or M)   Enter duty cycle for motor 1           
                                                         (n or N)   Enter duty cycle for motor 2           
                                                         (c or C)   Clear a fault message                  
                                                         (g or G)   Collect data for 30 seconds            
                                                         (t or T)   Begin testing interface                
                                                         (s or S)   End data collection                    
                                                         (h or H)   Display this help message 
                                                                       
        @param state    keeps track of the state of the system
    '''
    ##  @brief      Keeps track of which state the user task is in
    #   
    state = 0
        
    ##  @brief      next time code should run through primary if statement
    # 
    next_time = time.ticks_add(time.ticks_us() , period)

    ##  @brief      virtual com port
    # 
    ser = pyb.USB_VCP()
        
    while True:
        
        ##  @brief      Current time of the system
        #   
        current_time = time.ticks_us()
        
        if time.ticks_diff(current_time, next_time) >=0:
            
            next_time = time.ticks_add(next_time, period)
            
            if state == 0:
                printhelp()
                state = 1
                
                
            elif state == 1:
                if ser.any():
                    ##  @brief      Most recent character inputted to keyboard
                    # 
                    charIn = ser.read(1).decode()
                    
                    if charIn in {'z' , 'Z'}:
                        zFlag.write(True)
                        state = 2
                        
                    elif charIn in {'p' , 'P'}:
                        pFlag.write(True)
                        state = 3
                        
                    elif charIn in {'d' , 'D'}:
                        dFlag.write(True)
                        state = 4  
                        
                    elif charIn in {'g' , 'G'}:
                        gFlag.write(True)
                        print('Collecting data') 
                        
                        ##  @brief      Time data collection was started
                        # 
                        start_time = current_time
                        
                        ##  @brief      position data collected during time frame
                        # 
                        posdata = array.array('i' , 3001*[0])
                        
                        ##  @brief      time data collected during time frame
                        # 
                        timedata = array.array('i' , 3001*[0])
                     
                        ##  @brief      used to iterate
                        #   @details    Used in the iteration needed to change the values of the indexes
                        #               of posdata and timedata
                        # 
                        i = 0
                        
                        ##  @brief      used to iterate
                        #   @details    Used in the iteration needed to print the values
                        #               of posdata and timedata
                        # 
                        k = 0
                        
                        state = 5
                        
                    elif charIn in {'h' , 'H'}:
                        state = 0
                    
                    elif charIn in {'s' , 'S'}:
                        sFlag.write(True)
                        state = 1  
                        
                    elif charIn in {'e' , 'E'}:
                        eFlag.write(True)
                        print("Enabling motor.")
                        state = 1

                    elif charIn in {'m' , 'M'}:
                        if wFlag.read() == False:
                            ## @brief buffer to hold duty cycle input from user
                            #
                            buf = ''
                            print('Input duty cycle for motor 1')
                            state = 7   
                        else:
                            print('Closed loop control must be disabled.')

                    elif charIn in {'n' , 'N'}:
                        ## @brief buffer to hold duty cycle input from user
                        #
                        bufN = ''
                        print('Input duty cycle for motor 2')
                        state = 8
                        
                    elif charIn in {'t' , 'T'}:
                        if wFlag.read() == False:
                            print('Beginning data collection')
                            print('Input duty cycle for motor 1, press s to stop data collection')
                            ## @brief buffer to hold duty cycle input from user
                            #
                            bufT = ''
                            ## @brief velocity data during testing
                            #
                            velT = []
                            ## @brief duty cycle data during testing
                            #
                            dutT = []
                            state = 9
                        else:
                            print('Closed loop control must be disabled.')
                            
                    elif charIn in {'v' , 'V'}:
                        print(f'Velocity is {vel.read()} rad/s')
                        state = 1
                    
                    elif charIn in {'c', 'C'}:
                        cFlag.write(True)
                        print('Faults cleared')
                        state = 1
                        
                    elif charIn in {'y' , 'Y'}:
                        if wFlag.read() == True:
                            print('Enter set point for motor 1 (170 - -170) [rad/s]')
                            ## @brief buffer to hold set point input from user
                            #
                            bufY = ''
                            state = 12  
                        else:
                            print('Closed loop control must be enabled.')
                            state = 1
                    
                    elif charIn in {'k' , 'K'}:
                        if wFlag.read() == True:
                            print('Enter gain for motor 1 (0 - 0.5) [%]')
                            ## @brief buffer to hold Kp input from user
                            #
                            bufK = ''
                            state = 13  
                        else:
                            print('Closed loop control must be enabled.')
                            state = 1
                        
                    elif charIn in {'w' , 'W'}:
                        if wFlag.read() == False:
                            wFlag.write(True)
                            print("Enabling closed loop control.")

                        elif wFlag.read() == True:
                            wFlag.write(False)
                            print("Disabling closed loop control.")
                            state = 1  
                        
                    elif charIn in {'r' , 'R'}:
                        if wFlag.read() == True:
                            print('Enter gain for motor 1 (0 - 0.5) or press enter to exit')
                            bufK = ''
                            bufY = ''
                            order = 0
     
                            posdata = array.array('i' , 301*[0])
                            
                            timedata = array.array('i' , 301*[0])
                            
                            i = 0
                            
                            k = 0
                            
                            state = 11
                        else:
                            print('Closed loop control must be enabled.')
                            state = 1
                            
                        

                
                    
                        
                    else:
                        print(f"Input {charIn} not allowed")
                        
                        
            elif state == 2: #zeroing state
                if not zFlag.read():
                    print("The encoder has been zeroed")
                    state = 1
                    
                    
            elif state == 3: #printing position
                if not pFlag.read():
                    print(f"Time = {current_time/1_000_000}, Rads = {(position.read()/4000)*2*3.1415}")
                    state = 1
                    
            elif state == 4: #printing delt
                if not dFlag.read():
                    print(f"Time = {current_time/1_000_000}, Delta = {delta.read()}")
                    state = 1
                    
            elif state == 5: #starting data collection
                if gFlag.read():
                    
                    if current_time > 30_000_000 + start_time:
                    
                        gFlag.write(False)
                        state = 6       #go to print state
                    
                    elif ser.any():
                        charIn = ser.read(1).decode()
                        gFlag.write(False)
                        
                        if charIn in {'s' , 'S'}:
                            sFlag.write(True)
                            state = 6
                        
                    else:
                    
                        if i <= len(timedata): 
                            timedata[i] , posdata[i] = (current_time - start_time) , position.read()
                            i = i+1
                        
                        
                        
                    
                    
            elif state == 6: # Printing state
                if not gFlag.read():
                    if k < i:
                        p1 = posdata[k]
                        p2 = posdata[k + 1]
                        t1 = timedata[k]
                        t2 = timedata[k+1]
                        velocity = ((10**6)*(p2 - p1)*2*3.1415/4000)/(t2 - t1)
                        print(f"{timedata[k]/1000000:.2f}, {posdata[k]}, {velocity}")
                        k = k + 1
                    elif k == i:
                        state = 1
                
            elif state == 7: #taking user input for duty1 cycle
                
                if ser.any(): #check if char is inputed
                    charDuty = ser.read(1).decode() 
                    
                    if charDuty.isdigit(): #What to do if the char is a char
                        buf += charDuty 
                        ser.write(charDuty)
                        
                    elif charDuty == '-': #if it is minus
                        if len(buf) == 0:
                            buf += charDuty
                            ser.write(charDuty)
                            
                        else:
                            state = 7
                        
                    elif charDuty in {'\b','\x08', '\x7F'}: #If backspace is pressed
                        if len(buf) == 0:
                            state = 7
                            
                        else:
                            buf = buf[:-1]
                            ser.write(charDuty)
                        
                    elif charDuty in {'\r','\n'}: #If enter is pressed

                        if len(buf) == 0:
                            print('Motor duty cycle has not changed')
                            state = 1
                            
                        elif float(buf) in range(-101, 101):
                            duty1.write(float(buf))
                            print(f'\rDuty cycle for motor 1 set to {buf}%')
                            mFlag.write(True)
                            state = 1
                        
                        else:
                            print('\rInvalid duty cycle please enter a number between -100 and 100')
                            state = 1
                        
                    else:
                        state = 7 
                            
                
            elif state == 8: #taking user input for duty2 cycle
                if ser.any(): #check if char is inputed
                    charDutyN = ser.read(1).decode() 
                    
                    if charDutyN.isdigit(): #What to do if the char is a char
                        bufN += charDutyN 
                        ser.write(charDutyN)
                        
                    elif charDutyN == '-': #if it is minus
                        if len(bufN) == 0:
                            bufN += charDutyN
                            ser.write(charDutyN)
                            
                        else:
                            state = 8
                        
                    elif charDutyN in {'\b','\x08', '\x7F'}: #If backspace is pressed
                        if len(bufN) == 0:
                            state = 8
                            
                        else:
                            bufN = bufN[:-1]
                            ser.write(charDutyN)
                        
                    elif charDutyN in {'\r','\n'}: #If enter is pressed
                        
                        if len(bufN) == 0:
                            print('Motor duty cycle has not changed')
                            state = 1
                        
                        elif float(bufN) in range(-101, 101):
                            duty2.write(float(bufN))
                            print(f'\rDuty cycle for motor 2 set to {bufN}%')
                            nFlag.write(True)
                            state = 1
                                                
                        else:
                            print('\rInvalid duty cycle please enter a number between -100 and 100')
                            state = 1
                        
                    else:
                        state = 8 
                
                
            elif state == 9: #no escape unless s is pressed
                        
                if ser.any(): #check if char is inputed
                    charDutyT = ser.read(1).decode() 
                    
                    if charDutyT in {'s', 'S'}:
                        velT.append(vel.read())
                        print('Ending data collection')
                        print('+- Duty Cycle (%) -- Velocity (rad/s) -+')
                        sFlag.write(True)
                        L = 0
                        state = 10
                        
                    elif charDutyT in {'c','C'}: 
                        cFlag.write(True)
                        print('Faults cleared')
                        state = 9
                        
                    elif charDutyT.isdigit(): #What to do if the char is a char
                        bufT += charDutyT 
                        ser.write(charDutyT)
                        
                    elif charDutyT == '-': #if it is minus
                        if len(bufT) == 0:
                            bufT += charDutyT
                            ser.write(charDutyT)
                        
                    elif charDutyT in {'\b','\x08', '\x7F'}: #If backspace is pressed
                        if len(bufT) != 0:
                            bufT = bufT[:-1]
                            ser.write(charDutyT)
                        
                    elif charDutyT in {'\r','\n'}: #If enter is pressed
                        if len(bufT) == 0:
                            print('Motor duty cycle has not changed')
                            state = 9
                        
                        elif float(bufT) in range(-101, 101):
                            duty1.write(float(bufT))
                            print(f'\rDuty cycle for motor 1 set to {bufT}%')
                            print('Input duty cycle for motor 1, press s to stop data collection')
                            dutT.append(float(bufT))
                            velT.append(vel.read())
                            bufT =''
                            tFlag.write(True)
                            state = 9
                                                
                        else:
                            print('\rInvalid duty cycle please enter a number between -100 and 100')
                            state = 9
                        

                    else:
                        state = 9
            

                    
                
                        
                        
            elif state == 10: #printing motor vel and duty array
                if sFlag.read():
                    if L < len(dutT):
                        print(f"   {dutT[L]},       {velT[L+1]}")
                        L = L + 1
                    elif L == len(dutT):
                        state = 1
            
            elif state == 11:       #r state

                if order == 0:
                    #run k states code
                    if ser.any(): #check if char is inputed
                        charDutyK = ser.read(1).decode() 
                        
                        if charDutyK.isdigit(): #What to do if the char is a char
                            bufK += charDutyK 
                            ser.write(charDutyK)
                            
                        elif charDutyK == '.': #if it is decimal 
                            if '.' in bufK:
                                state = 11
                                
                            else:
                                bufK += charDutyK
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufK) == 0:
                                state = 11
                                
                            else:
                                bufK = bufK[:-1]
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufK) == 0:
                                print('Gain has not changed')
                                state = 1
                            
                            elif float(bufK) >= 0 and float(bufK) <= 0.7:
                                Kp_user = float(bufK)
                                print(f'\rGain set to {bufK}%')
                                print('Enter set point for motor 1 (170 - -170) [rad/s]')
                                order = 1
                                                    
                            else:
                                print('\rInvalid gain please choose a value between 0 and 0.5')
                                bufK = ''
                                order = 0
                    
                elif order == 1:
                    #run y states code
                    if ser.any(): #check if char is inputed
                        charDutyY = ser.read(1).decode() 
                        
                        if charDutyY.isdigit(): #What to do if the char is a char
                            bufY += charDutyY 
                            ser.write(charDutyY)
                            
                        elif charDutyY == '-': #if it is minus
                            if len(bufY) == 0:
                                bufY += charDutyY
                                ser.write(charDutyY)
                                
                            else:
                                state = 11
                            
                        elif charDutyY in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufY) == 0:
                                state = 11
                            
                            else:
                                bufY = bufY[:-1]
                                ser.write(charDutyY)
                            
                        elif charDutyY in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufY) == 0:
                                print('Set point has not changed')
                                state = 11
                            
                            elif float(bufY) in range(-171, 171):
                                ref_val.write(float(bufY))
                                print(f'\rSet point set to {bufY}rad/s')
                                print('Beginning data collection, press s to stop early')
                                order = 2
                                                    
                            else:
                                print('\rInvalid set point please choose a value between -170 and 170')
                                bufY = ''
                                order = 1
                    
                
                elif order == 2:
                    start_time = current_time
                    order = 3
                
                elif order == 3:
                    if ser.any(): #check if char is inputed
                         charDuty = ser.read(1).decode()
                         if charDuty in {'s' or 'S'}:
                             print('Ending data collection.')
                             print('Time(s) Actuation lvl(%) Velocity(rad/s)')
                             state =14
                             
                    elif current_time > 3_000_000 + start_time:
                        state = 14       #go to print state
                    
                    elif current_time < 1_000_000 + start_time:
                        Kp.write(0)
                        if i <= len(timedata): 
                            timedata[i] , posdata[i] = (current_time - start_time) , position.read()
                            i = i+1
                        
                    else:
                        Kp.write(Kp_user)
                        if i <= len(timedata): 
                            timedata[i] , posdata[i] = (current_time - start_time) , position.read()
                            i = i+1
                
            elif state == 12: #y state

                    if ser.any(): #check if char is inputed
                        charDutyY = ser.read(1).decode() 
                        
                        if charDutyY.isdigit(): #What to do if the char is a char
                            bufY += charDutyY 
                            ser.write(charDutyY)
                            
                        elif charDutyY == '-': #if it is minus
                            if len(bufY) == 0:
                                bufY += charDutyY
                                ser.write(charDutyY)
                                
                            else:
                                state = 12
                            
                        elif charDutyY in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufY) == 0:
                                state = 12
                                
                            else:
                                bufY = bufY[:-1]
                                ser.write(charDutyY)
                            
                        elif charDutyY in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufY) == 0:
                                print('Set point has not changed')
                                state = 1
                            
                            elif float(bufY) in range(-171, 171):
                                ref_val.write(float(bufY))
                                print(f'\rSet point set to {bufY}rad/s')
                                state = 1
                                                    
                            else:
                                print('\rInvalid set point please choose a value between -170 and 170')
                                state = 1
                
            elif state == 13: #k state
  
                if ser.any(): #check if char is inputed
                        charDutyK = ser.read(1).decode() 
                        
                        if charDutyK.isdigit(): #What to do if the char is a char
                            bufK += charDutyK 
                            ser.write(charDutyK)
                            
                        elif charDutyK == '.': #if it is decimal 
                            if '.' in bufK:
                                state = 13
                                
                            else:
                                bufK += charDutyK
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufK) == 0:
                                state = 13
                                
                            else:
                                bufK = bufK[:-1]
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufK) == 0:
                                print('Gain has not changed')
                                state = 1
                            
                            elif float(bufK) >= 0 and float(bufK) <= 0.7:
                                Kp.write(float(bufK))
                                print(f'\rGain set to {bufK}%')
                                state = 1
                                                    
                            else:
                                print('\rInvalid gain please choose a value between 0 and 0.5')
                                state = 1

                            
            elif state == 14: # Printing state for R state
                
                if k < i - 1:
                    p1 = posdata[k]
                    p2 = posdata[k + 1]
                    t1 = timedata[k]
                    t2 = timedata[k+1]
                    velocity = ((10**6)*(p2 - p1)*2*-3.1415/4000)/(t2 - t1)
                    velPercent = (velocity/ref_val.read())*100
                    print(f"{timedata[k]/1000000:.2f}, {velPercent}, {velocity}")
                    k = k + 1
                elif k == i-1:
                    state = 1
                
                        
            else:
                
                raise ValueError(f"invalid state in {taskName}")
            
            next_time = time.ticks_add(next_time , period)
            
            yield state
            
        else:
            yield None
                

def printhelp():
    '''!@brief      Prints the user interface menu
        @details    A series of print states that forms the user interface menu
    '''
    
    print("+---------------------------------------------------+")
    print("|                Main Menu                          |")
    print("| (z or Z)   Zero the encoder                       |")
    print("| (p or P)   Print encoder position                 |")
    print("| (d or D)   Print encoder delta                    |")
    print("| (v or V)   Print encoder velocity                 |")
    print("| (e or E)   Enable motors                          |")
    print("| (y or Y)   Enter set point for motor 1            |")
    print("| (k or K)   Enter gain for motor 1                 |")
    print("| (w or W)   Enable or disable closed Loop control  |")
    print("| (r or R)   Perform step response on motor 1       |")
    print("| (m or M)   Enter duty cycle for motor 1           |")
    print("| (n or N)   Enter duty cycle for motor 2           |")
    print("| (c or C)   Clear a fault message                  |")
    print("| (g or G)   Collect data for 30 seconds            |")
    print("| (t or T)   Begin testing interface                |")
    print("| (s or S)   End data collection                    |")
    print("| (h or H)   Display this help message              |")
    print("+---------------------------------------------------+")