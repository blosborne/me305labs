'''!    @file user_task.py
        @brief                                  Generates and manages a user interface for the encoder
        @details                                The user interface has the following functions;
                                                    (1)     Zeroing the encoder
                                                    (2)     Printing encoder position
                                                    (3)     Printing encoder delta
                                                    (4)     Collecting position data for 30 seconds
                                                    (5)     Stopping data collection
                                                    (6)     Printing a help menu
                                                
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   January 27, 2022
'''

import time , pyb , array

def taskUserFcn (taskName , period , zFlag, pFlag, dFlag, gFlag, sFlag, position, delta):
    '''!@brief          Generates and manages a user interface for the encoder
        @details        The user interface has the following functions;
                            (1)     Zeroing the encoder
                            (2)     Printing encoder position
                            (3)     Printing encoder delta
                            (4)     Collecting position data for 30 seconds
                            (5)     Stopping data collection
                            (6)     Printing a help menu
                                                        
        @param state    keeps track of the state of the system
    '''
    ##  @brief      Keeps track of which state the user task is in
    #   
    state = 0
    next_time = time.ticks_add(time.ticks_us() , period)

    
    ser = pyb.USB_VCP()
        
    while True:
        
        ##  @brief      Current time of the system
        #   
        current_time = time.ticks_us()
        
        if time.ticks_diff(current_time, next_time) >=0:
            
            next_time = time.ticks_add(next_time, period)
            
            if state == 0:
                printhelp()
                state = 1
                
                
            elif state == 1:
                if ser.any():
                    ##  @brief      Most recent character inputted to keyboard
                    # 
                    charIn = ser.read(1).decode()
                    
                    if charIn in {'z' , 'Z'}:
                        zFlag.write(True)
                        state = 2
                        
                    elif charIn in {'p' , 'P'}:
                        pFlag.write(True)
                        state = 3
                        
                    elif charIn in {'d' , 'D'}:
                        dFlag.write(True)
                        state = 4  
                        
                    elif charIn in {'g' , 'G'}:
                        gFlag.write(True)
                        print('Collecting data') 
                        
                        ##  @brief      Time data collection was started
                        # 
                        start_time = current_time
                        
                        ##  @brief      position data collected during time frame
                        # 
                        posdata = array.array('l' , 3001*[0])
                        
                        ##  @brief      time data collected during time frame
                        # 
                        timedata = array.array('l' , 3001*[0])
                        
                        ##  @brief      used to iterate
                        #   @details    Used in the iteration needed to change the values of the indexes
                        #               of posdata and timedata
                        # 
                        i = 0
                        
                        ##  @brief      used to iterate
                        #   @details    Used in the iteration needed to print the values
                        #               of posdata and timedata
                        # 
                        k = 0
                        
                        state = 5
                        
                    elif charIn in {'h' , 'H'}:
                        state = 0
                    
                    elif charIn in {'s' , 'S'}:
                        sFlag.write(True)
                        state = 1  
                        
                    else:
                        print(f"Input {charIn} not allowed")
                        
                        
            elif state == 2: #zeroing state
                if not zFlag.read():
                    print("The encoder has been zeroed")
                    state = 1
                    
                    
            elif state == 3: #printing position
                if not pFlag.read():
                    print(f"Time = {current_time/1_000_000}, Ticks = {position.read()}")
                    state = 1
                    
            elif state == 4: #printing delt
                if not dFlag.read():
                    print(f"Time = {current_time/1_000_000}, Delta = {delta.read()}")
                    state = 1
                    
            elif state == 5: #starting data collection
                if gFlag.read():
                    
                    if current_time > 30_000_000 + start_time:
                    
                        gFlag.write(False)
                        state = 6       #go to print state
                    
                    elif ser.any():
                        charIn = ser.read(1).decode()
                        gFlag.write(False)
                        
                        if charIn in {'s' , 'S'}:
                            sFlag.write(True)
                            state = 6
                        
                    else:
                    
                        if i <= len(timedata): 
                            timedata[i] , posdata[i] = (current_time - start_time) , position.read()
                            i = i+1
                        
                        
                        
                    
                    
            elif state == 6: # Printing state
                if not gFlag.read():
                    if k < i:
                        
                        print(f"{timedata[k]/1000000:.2f}, {posdata[k]}")
                        k = k + 1
                    elif k == i:
                        state = 1
                
                    
            else:
                
                raise ValueError(f"invalid state in {taskName}")
            
            next_time = time.ticks_add(next_time , period)
            
            yield state
            
        else:
            yield None
                

def printhelp():
    '''!@brief      Prints the user interface menu
        @details    A series of print states that forms the user interface menu
    '''
    
    print("+----------------------------------------+")
    print("|                Main Menu               |")
    print("| (z or Z)   zero the encoder            |")
    print("| (p or P)   print encoder position      |")
    print("| (d or D)   print encoder delta         |")
    print("| (g or G)   collect data for 30 seconds |")
    print("| (s or S)   end data collection         |")
    print("| (h or H)   display this help message   |")
    print("+----------------------------------------+")