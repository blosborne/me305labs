'''!    @file main.py
        @brief                                  Runs the following tasks; user_task.py, encoder_task.py
        @details                                Initializes shares and then run both user_task.py and encoder_task.py
                                                
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   January 27, 2022
'''

import user_task , encoder_task , shares

##  @brief      Flag to show that the user pressed 'z'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'z'
#               encoder_task.py will make this value false after the encoder is zeroed
#
zFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'p'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'p'
#               encoder_task.py will make this value false after the encoder position is acquired
#
pFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'd'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'd'
#               encoder_task.py will make this value false after the encoder delta is acquired
#
dFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'g'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'g'
#               encoder_task.py will make this value false after the encoder data is acquired
#
gFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 's'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 's'
#               encoder_task.py will make this value false after the encoder data collection is stopped
#
sFlag = shares.Share(False)

##  @brief      A shared variable used to store encoder position
#   @details    encoder_task.py will give this variable a value and user_task.py will print this value
#
position = shares.Share(0)

##  @brief      A shared variable used to store encoder delta
#   @details    encoder_task.py will give this variable a value and user_task.py will print this value
#
delta = shares.Share(0)

task1 = user_task.taskUserFcn ("T1" , 10000 , zFlag, pFlag, dFlag, gFlag, sFlag, position, delta)
task2 = encoder_task.taskEncoderFcn("T2" , 10000 , zFlag, pFlag, dFlag, gFlag, sFlag, position, delta)

tasklist = [task1 , task2]

if __name__  == '__main__':
    
    while True:
    
        try:
            for task in tasklist:
                next(task)
                
        except KeyboardInterrupt:
            break
    
    

    print("Program Terminating")
        
        
        

