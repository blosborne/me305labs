'''!    @file user_task.py
        @brief                                  Generates and manages a user interface for the encoder
        @details                                The user interface has the following functions;
                                                    (1)     Zeroing the encoder
                                                    (2)     Printing encoder position
                                                    (3)     Printing encoder delta
                                                    (4)     Collecting position data for 30 seconds
                                                    (5)     Stopping data collection
                                                    (6)     Printing a help menu
                                                
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   January 27, 2022
'''

import time , pyb , array

def taskUserFcn (taskName , period , zFlag, pFlag, dFlag, gFlag, sFlag, eFlag, nFlag, mFlag, tFlag, cFlag, position, delta, duty1, duty2, vel):
    '''!@brief          Generates and manages a user interface for the encoder
        @details        The user interface has the following functions;
                            (1)     Zeroing the encoder
                            (2)     Printing encoder position
                            (3)     Printing encoder delta
                            (4)     Collecting position data for 30 seconds
                            (5)     Stopping data collection
                            (6)     Printing a help menu
                                                        
        @param state    keeps track of the state of the system
    '''
    ##  @brief      Keeps track of which state the user task is in
    #   
    state = 0
        
    ##  @brief      next time code should run through primary if statement
    # 
    next_time = time.ticks_add(time.ticks_us() , period)

    ##  @brief      virtual com port
    # 
    ser = pyb.USB_VCP()
        
    while True:
        
        ##  @brief      Current time of the system
        #   
        current_time = time.ticks_us()
        
        if time.ticks_diff(current_time, next_time) >=0:
            
            next_time = time.ticks_add(next_time, period)
            
            if state == 0:
                printhelp()
                state = 1
                
                
            elif state == 1:
                if ser.any():
                    ##  @brief      Most recent character inputted to keyboard
                    # 
                    charIn = ser.read(1).decode()
                    
                    if charIn in {'z' , 'Z'}:
                        zFlag.write(True)
                        state = 2
                        
                    elif charIn in {'p' , 'P'}:
                        pFlag.write(True)
                        state = 3
                        
                    elif charIn in {'d' , 'D'}:
                        dFlag.write(True)
                        state = 4  
                        
                    elif charIn in {'g' , 'G'}:
                        gFlag.write(True)
                        print('Collecting data') 
                        
                        ##  @brief      Time data collection was started
                        # 
                        start_time = current_time
                        
                        ##  @brief      position data collected during time frame
                        # 
                        posdata = array.array('i' , 3001*[0])
                        
                        ##  @brief      time data collected during time frame
                        # 
                        timedata = array.array('i' , 3001*[0])
                     

                       # veldata = array.array('i', 3001*[0])
                        
                        ##  @brief      used to iterate
                        #   @details    Used in the iteration needed to change the values of the indexes
                        #               of posdata and timedata
                        # 
                        i = 0
                        
                        ##  @brief      used to iterate
                        #   @details    Used in the iteration needed to print the values
                        #               of posdata and timedata
                        # 
                        k = 0
                        
                        state = 5
                        
                    elif charIn in {'h' , 'H'}:
                        state = 0
                    
                    elif charIn in {'s' , 'S'}:
                        sFlag.write(True)
                        state = 1  
                        
                    elif charIn in {'e' , 'E'}:
                        eFlag.write(True)
                        print('Enabling Motors')
                        state = 1
                        
                    elif charIn in {'m' , 'M'}:
                        buf = ''
                        print('Input duty cycle for motor 1')
                        state = 7                        

                    elif charIn in {'n' , 'N'}:
                        bufN = ''
                        print('Input duty cycle for motor 2')
                        state = 8
                        
                    elif charIn in {'t' , 'T'}:
                        print('Beginning data collection')
                        print('Input duty cycle for motor 1, press s to stop data collection')
                        bufT = ''
                        velT = []
                        dutT = []
                        state = 9
                        
                    elif charIn in {'v' , 'V'}:
                        print(f'Velocity is {vel.read()} rad/s')
                        state = 1
                    
                    elif charIn in {'c', 'C'}:
                        cFlag.write(True)
                        print('Faults cleared')
                        state = 1
                
                    
                        
                    else:
                        print(f"Input {charIn} not allowed")
                        
                        
            elif state == 2: #zeroing state
                if not zFlag.read():
                    print("The encoder has been zeroed")
                    state = 1
                    
                    
            elif state == 3: #printing position
                if not pFlag.read():
                    print(f"Time = {current_time/1_000_000}, Rads = {(position.read()/4000)*2*3.1415}")
                    state = 1
                    
            elif state == 4: #printing delt
                if not dFlag.read():
                    print(f"Time = {current_time/1_000_000}, Delta = {delta.read()}")
                    state = 1
                    
            elif state == 5: #starting data collection
                if gFlag.read():
                    
                    if current_time > 30_000_000 + start_time:
                    
                        gFlag.write(False)
                        state = 6       #go to print state
                    
                    elif ser.any():
                        charIn = ser.read(1).decode()
                        gFlag.write(False)
                        
                        if charIn in {'s' , 'S'}:
                            sFlag.write(True)
                            state = 6
                        
                    else:
                    
                        if i <= len(timedata): 
                           # timedata[i] , posdata[i], veldata[i] = (current_time - start_time) , position.read(), vel.read()
                            timedata[i] , posdata[i] = (current_time - start_time) , position.read()
                            i = i+1
                        
                        
                        
                    
                    
            elif state == 6: # Printing state
                if not gFlag.read():
                    if k < i:
                        p1 = posdata[k]
                        p2 = posdata[k + 1]
                        t1 = timedata[k]
                        t2 = timedata[k+1]
                        velocity = (2*(10**6)*(p2 - p1)*2*3.1415/4000)/(t2 - t1)
                        print(f"{timedata[k]/1000000:.2f}, {posdata[k]}, {velocity}")
                        k = k + 1
                    elif k == i:
                        state = 1
                
            elif state == 7: #taking user input for duty1 cycle
                
                if ser.any(): #check if char is inputed
                    charDuty = ser.read(1).decode() 
                    
                    if charDuty.isdigit(): #What to do if the char is a char
                        buf += charDuty 
                        ser.write(charDuty)
                        #state = 7
                        
                    elif charDuty == '-': #if it is minus
                        if len(buf) == 0:
                            buf += charDuty
                            ser.write(charDuty)
                            
                        else:
                            state = 7
                        
                    elif charDuty in {'\b','\x08', '\x7F'}: #If backspace is pressed
                        if len(buf) == 0:
                            state = 7
                            
                        else:
                            buf = buf[:-1]
                            ser.write(charDuty)
                        
                    elif charDuty in {'\r','\n'}: #If enter is pressed
                        #dutyIn = float(buf)
                        
                        if len(buf) == 0:
                            print('Motor duty cycle has not changed')
                            state = 1
                            
                        elif float(buf) in range(-101, 101):
                            #dutyIn = float(buf)
                            duty1.write(float(buf))
                            print(f'\rDuty cycle for motor 1 set to {buf}')
                            mFlag.write(True)
                            state = 1
                        
                        else:
                            print('\rInvalid duty cycle please enter a number between -100 and 100')
                            state = 1
                        
                    else:
                        state = 7 
                            
                
            elif state == 8: #taking user input for duty2 cycle
                if ser.any(): #check if char is inputed
                    charDutyN = ser.read(1).decode() 
                    
                    if charDutyN.isdigit(): #What to do if the char is a char
                        bufN += charDutyN 
                        ser.write(charDutyN)
                        #state = 8
                        
                    elif charDutyN == '-': #if it is minus
                        if len(bufN) == 0:
                            bufN += charDutyN
                            ser.write(charDutyN)
                            
                        else:
                            state = 8
                        
                    elif charDutyN in {'\b','\x08', '\x7F'}: #If backspace is pressed
                        if len(bufN) == 0:
                            state = 8
                            
                        else:
                            bufN = bufN[:-1]
                            ser.write(charDutyN)
                        
                    elif charDutyN in {'\r','\n'}: #If enter is pressed
                        #dutyInN = float(bufN)
                        
                        if len(bufN) == 0:
                            print('Motor duty cycle has not changed')
                            state = 1
                        
                        elif float(bufN) in range(-101, 101):
                            #dutyInN = float(bufN)
                            duty2.write(float(bufN))
                            print(f'\rDuty cycle for motor 2 set to {bufN}')
                            nFlag.write(True)
                            state = 1
                                                
                        else:
                            print('\rInvalid duty cycle please enter a number between -100 and 100')
                            state = 1
                        
                    else:
                        state = 8 
                
                
            elif state == 9: #no escape unless s is pressed
                        
                if ser.any(): #check if char is inputed
                    charDutyT = ser.read(1).decode() 
                    
                    if charDutyT in {'s', 'S'}:
                        velT.append(vel.read())
                        print('Ending data collection')
                        print('+- Duty Cycle (%) -- Velocity (rad/s) -+')
                        sFlag.write(True)
                        L = 0
                        state = 10
                        
                    elif charDutyT in {'c','C'}: 
                        cFlag.write(True)
                        print('Faults cleared')
                        state = 9
                        
                    elif charDutyT.isdigit(): #What to do if the char is a char
                        bufT += charDutyT 
                        ser.write(charDutyT)
                        
                    elif charDutyT == '-': #if it is minus
                        if len(bufT) == 0:
                            bufT += charDutyT
                            ser.write(charDutyT)
                        
                    elif charDutyT in {'\b','\x08', '\x7F'}: #If backspace is pressed
                        if len(bufT) != 0:
                            bufT = bufT[:-1]
                            ser.write(charDutyT)
                        
                    elif charDutyT in {'\r','\n'}: #If enter is pressed
                        if len(bufT) == 0:
                            print('Motor duty cycle has not changed')
                            state = 9
                        
                        elif float(bufT) in range(-101, 101):
                            #dutyInN = float(bufN)
                            duty1.write(float(bufT))
                            print(f'\rDuty cycle for motor 1 set to {bufT}')
                            print('Input duty cycle for motor 1, press s to stop data collection')
                            dutT.append(float(bufT))
                            velT.append(vel.read())
                            bufT =''
                            tFlag.write(True)
                            state = 9
                                                
                        else:
                            print('\rInvalid duty cycle please enter a number between -100 and 100')
                            state = 9
#                        dutyInT = float(bufT)
#                        duty1.write(dutyInT)
#                        print(f'\rDuty cycle for motor 1 set to {dutyInT}')
#                        print('Input duty cycle for motor 1, press s to stop data collection')
#                        dutT.append(dutyInT)
#                        velT.append(vel.read())
#                        bufT = ''
#                        tFlag.write(True)
#                        state = 9
                        

                    else:
                        state = 9
            

                    
                
                        
                        
            elif state == 10: #printing motor vel and duty array
                if sFlag.read():
                    if L < len(dutT):
                        print(f"   {dutT[L]},       {velT[L+1]}")
                        L = L + 1
                    elif L == len(dutT):
                        state = 1
                    
                
            else:
                
                raise ValueError(f"invalid state in {taskName}")
            
            next_time = time.ticks_add(next_time , period)
            
            yield state
            
        else:
            yield None
                

def printhelp():
    '''!@brief      Prints the user interface menu
        @details    A series of print states that forms the user interface menu
    '''
    
    print("+-----------------------------------------+")
    print("|                Main Menu                |")
    print("| (z or Z)   Zero the encoder             |")
    print("| (p or P)   Print encoder position       |")
    print("| (d or D)   Print encoder delta          |")
    print("| (v or V)   Print encoder velocity       |")
    print("| (e or E)   Enable motors                |")
    print("| (m or M)   Enter duty cycle for motor 1 |")
    print("| (n or N)   Enter duty cycle for motor 2 |")
    print("| (c or C)   Clear a fault message        |")
    print("| (g or G)   Collect data for 30 seconds  |")
    print("| (t or T)   Begin testing interface      |")
    print("| (s or S)   End data collection          |")
    print("| (h or H)   Display this help message    |")
    print("+-----------------------------------------+")