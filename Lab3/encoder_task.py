'''!    @file encoder_task.py
        @brief                                  Serves as a medium between user_task.py and encoder.py
        @details                                Facillitates communication between user_task.py and the encoder driver,
                                                encoder.py. This code will recieve signals from user_task and go and get
                                                information from the encoder driver.
                                                
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   January 27, 2022
'''

import time, pyb, encoder

def taskEncoderFcn(taskName , period , zFlag, pFlag, dFlag, gFlag, sFlag, position, delta, vel):
    '''!@brief          Serves as a medium between user_task.py and encoder.py
        @details        Facillitates communication between user_task.py and the encoder driver,
                        encoder.py. This code will recieve signals from user_task and go and get
                        information from the encoder driver.
                        
        @param state    keeps track of the state of the system
        @param colArray
    
    '''

    ##  @brief      Keeps track of which state the encoder task is in
    #   
    state = 0
    
    ##  @brief      next time that task will need to move on
    #   
    next_time = time.ticks_add(time.ticks_us() , period)
    
    
    pinCH1 = pyb.Pin (pyb.Pin.cpu.B6)
    pinCH2 = pyb.Pin (pyb.Pin.cpu.B7)
    myEncoder1 = encoder.Encoder(pinCH1, pinCH2, 4)   



    while True:
            
            current_time = time.ticks_us()
            position.write(myEncoder1.get_position())

            
            if time.ticks_diff(current_time, next_time) >=0:
                #delta.write(myEncoder1.get_delta())
                next_time = time.ticks_add(next_time, period)
                
                if state == 0:
                    state = 1
                    
                elif state == 1:    #zero state
                    
                    myEncoder1.update()
                    deltaVel = myEncoder1.get_delta()*2*3.1415/(4000*period*10**(-6))
                    vel.write(deltaVel)
                    
                    if zFlag.read():
                        myEncoder1.zero()
                        zFlag.write(False)
                    
                    elif pFlag.read():
                        position.write(myEncoder1.get_position())
                        pFlag.write(False)
                        
                    elif dFlag.read():
                        delta.write(myEncoder1.get_delta())
                        dFlag.write(False)
                    else:
                        delta.write(myEncoder1.get_delta())
                    
                else:
                    raise ValueError(f"invalid state in {taskName}")
                
                next_time = time.ticks_add(next_time , period)
                
                yield state
                
            else:
                yield None