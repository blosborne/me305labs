'''!    @file encoder.py
        @brief                                  A driver for reading from Quadrature Encoders
        @details                                A driver that can read from an encoder and provide position information.
                                                The driver can also provide a position difference. 
                                                The max CPR*w value this driver works for is BLANK.
                                                Where CPR is the resolution of the encoder in [ticks/rev]
                                                and w is the angular speed in [RPM]
                                                
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   January 20, 2022
'''

# import statements
import pyb



class Encoder:
    '''!    @brief                              Interface with quadrature encoders
            @details                            Can read from an encoder and provide position information.
                                                Can also provide a position difference.
    '''
    
    
    
    def __init__(self , pinA , pinB , timNum):
        '''!    @brief                          Constructs an encoder object
                @details                        Initiates a timer and sets initial conditions
        '''
        self.tim = pyb.Timer(timNum, prescaler = 0 , period = ((2**16) - 1) )
        self.tim.channel(1 , self.tim.ENC_AB , pin = pinA)
        self.tim.channel(2 , self.tim.ENC_AB , pin = pinB)
        
        ##  @brief      previous position of encoder
        #   @details    Used in calculation as an previous position
        #
        self.p0 = 0
        
        ##  @brief      next position of encoder
        #   @details    Used in calculation as an next position
        #
        self.p1 = 0
        
        ##  @brief      position of encoder
        #   @details    Stores the position calculated by this driver
        #
        self.position = 0
        print("Creating encoder object")
        
    def update(self):
        '''!    @brief                          Updates encoder position and delta
                @details                        The bulk of the calculations are done in this function. It computes delta by
                                                finding the difference between the current position and the last position.
                                                Then uses this delta to calculate an absolute position.
        '''
        
        self.p1 = self.tim.counter()      # going to replace position with 0 + delta 
        
        ##  @brief      The difference between the next position and previous position
        #   @details    This gives the change of position of the encoder. 
        #               This will also be summed continuously to calculate position
        #
        self.delta = self.p1 - self.p0
        
        if self.delta > 32768:          # positive overflow check
            
            self.delta -= (65536)           # overflow correction
            
        elif self.delta < -32768:     # negative overflow check
            
            self.delta += (65536)           # overflow correction
            
        else:
            
            self.delta = self.delta
        
        self.position = self.position + self.delta
        
        self.p0 = self.p1
        
        
    def get_position(self):
        '''!    @brief                          Returns encoder position
                @details                        Recieves the encoder position from the update function and provides that position
                @return                         The position of the encoder shaft
        '''
        return self.position

    def zero(self):
        '''     @brief                          Resets the encoder position to zero
                @details                        After the zero function is used the position of the encoder will reset to a value of zero
        '''
        self.position = 0
        self.p0 = self.tim.counter()
        
        
        
    def get_delta(self):
        '''!    @brief                          Returns encoder delta
                @details                        Recieves the encoder delta from the update function and provides that delta
                @return                         The change in position of the encoder shaft between the two most recent updates
        '''
        return self.delta

    
