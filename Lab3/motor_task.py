'''!@file motor_task.py
    @brief          Creates motor object using DRV8847.py
    @details        Uses DRV8847.py to create a motor controller object (DRV8847).
                    Also creates two motors using DRV8847.py. Facillitates communication
                    between suer_task.py and the physical motors.
    @author         O'Connell, Joseph
    @author         Osborne, Barrett
    @date           February 3, 2022

'''

import time , pyb, DRV8847

def taskMotorFcn(taskName, period, eFlag, nFlag, mFlag, cFlag, sFlag, tFlag, duty1, duty2):
    
    ##  @brief      Keeps track of which state the encoder task is in
    # 
    state = 1
    
    ##  @brief      next time that task will need to move on
    #   
    next_time = time.ticks_add(time.ticks_us() , period) 
   
    ##  @brief      DRV8847 object
    #   
    motor_drv = DRV8847.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2)
    
    ##  @brief      motor object 1
    #   
    motor_1 = motor_drv.motor(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2)
    
    ##  @brief      motor object 2
    # 
    motor_2 = motor_drv.motor(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4)

    while True:
        
        ##  @brief      courrent time of motor_task.py
        # 
        current_time = time.ticks_us()
        

        if time.ticks_diff(current_time, next_time) >=0:
            
            next_time = time.ticks_add(next_time, period)
                
            if state == 1:    
                
                if eFlag.read():
                    motor_drv.enable()
                    eFlag.write(False)
                
                elif nFlag.read():
                    motor_2.set_duty(-1*duty2.read())
                    nFlag.write(False)
                    
                elif mFlag.read():
                    motor_1.set_duty(-1*duty1.read())
                    mFlag.write(False)
                    
                elif tFlag.read():
                    motor_1.set_duty(-1*duty1.read())
                    tFlag.write(False)
                    
                elif cFlag.read():
                    motor_drv.enable()
                    motor_1.set_duty(0)
                    motor_2.set_duty(0)
                    cFlag.write(False)
                
            else:
                raise ValueError(f"invalid state in {taskName}")
            
            next_time = time.ticks_add(next_time , period)
            
            yield state
            
        else:
            yield None
        