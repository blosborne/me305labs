'''!@file DRV8847.py
    @brief          Creates DRV8847 object
    @details        Creates a DRV8847 object. This is a motor controller. This has the following functions;
                        enable the DRV8847
                        disable the DRV8847
                        detecting faults
                        creating a motor object
                        
    @author         O'Connell, Joseph
    @author         Osborne, Barrett
    @date           February 3, 2022

'''
from motor import Motor
from pyb import Pin, Timer, ExtInt
import time

class DRV8847:
    '''!@brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or moreobjects of the
                    Motor class which can be used to perform motor
                    control.
    
                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__ (self, SLEEP_pin, FAULT_pin):
        '''!@brief Initializes and returns a DRV8847 object.
        
            @param SLEEP_pin    DRV8847 sleep pin
            @param FAULT_pin    DRV8847 fault pin
        '''
        
        self.PWM_tim = Timer(3, freq = 20_000)
        self.nSLEEP = Pin(SLEEP_pin, mode=Pin.OUT_PP)
        self.nFAULT = Pin(FAULT_pin, mode=Pin.OUT_PP)
        self.MotorInt = ExtInt(self.nFAULT, mode = ExtInt.IRQ_FALLING, pull=Pin.PULL_NONE, callback = self.fault_cb)
        
        pass
    
    def enable (self):
        '''!@brief Brings the DRV8847 out of sleep mode.
        '''
        self.MotorInt.disable()
        self.nSLEEP.high()
        time.sleep_us(50)
        self.MotorInt.enable()
    
        pass
    
    def disable (self):
        '''!@brief Puts the DRV8847 in sleep mode.
        '''
        self.nSLEEP.low()
        pass
    
    def fault_cb (self, IRQ_src):
        '''!@brief Callback function to run on fault condition.
            @param IRQ_src The source of the interrupt request.
        '''
        #try button for testing
        self.disable()
        print('Motor Fault Detected! Disabling Motors. Press C to clear.')
    
    def motor (self, pin1, pin2, chNum_1, chNum_2):
        '''!@brief Creates a DC motor object connected to the DRV8847.
            @return An object of class Motor
        '''
        self.Motor = Motor(self.PWM_tim, pin1, pin2, chNum_1, chNum_2)
        
        return self.Motor

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    
    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
    motor_drv = DRV8847(Pin.cpu.A15, Pin.cpu.B2)
    motor_1 = motor_drv.motor(Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    motor_2 = motor_drv.motor(Pin.cpu.B0, Pin.cpu.B1, 3, 4)
    
    # Enable the motor driver
    motor_drv.enable()
    
    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    motor_1.set_duty(40)
    motor_2.set_duty(0)