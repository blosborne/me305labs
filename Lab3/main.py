'''!    @file main.py
        @brief                                  Runs the following tasks; user_task.py, encoder_task.py
        @details                                Initializes shares and then runs;
                                                    user_task.py
                                                    encoder_task.py
                                                    motor_task.py
                                                
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   January 27, 2022
'''

import user_task , encoder_task , shares, motor_task, DRV8847

##  @brief      Flag to show that the user pressed 'z'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'z'
#               encoder_task.py will make this value false after the encoder is zeroed
#
zFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'p'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'p'
#               encoder_task.py will make this value false after the encoder position is acquired
#
pFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'd'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'd'
#               encoder_task.py will make this value false after the encoder delta is acquired
#
dFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'g'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'g'
#               encoder_task.py will make this value false after the encoder data is acquired
#
gFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 's'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 's'
#               encoder_task.py will make this value false after the encoder data collection is stopped
#
sFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'e'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'e'
#               used to tell motor_task.py to enable the motors
#
eFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'n'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'n'
#               Used to throw system intoa state inwhich it collects a duty cycle value from the user
#
nFlag = shares.Share(False)


##  @brief      Flag to show that the user pressed 'm'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'm'
#               Used to throw system intoa state inwhich it collects a duty cycle value from the user
#
mFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'c'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'c'
#               Used to tell motor_task.py to clear the faults that have occured in the motor
#
cFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'n'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'n'
#               Used to throw system into a state inwhich it collects a duty cycle value
#               from the user repeatedly
#
tFlag = shares.Share(False)

##  @brief      A shared variable used to store encoder position
#   @details    encoder_task.py will give this variable a value and user_task.py will print this value
#
position = shares.Share(0)

##  @brief      A shared variable used to store encoder delta
#   @details    encoder_task.py will give this variable a value and user_task.py will print this value
#
delta = shares.Share(0)

##  @brief      A shared variable used to store duty cycle of motor 1
#
duty1 = shares.Share(0)

##  @brief      A shared variable used to store duty cycle of motor 2
#
duty2 = shares.Share(0)

##  @brief      A shared variable used to store the angular velocity of motor 1
#
vel = shares.Share(0)


task1 = user_task.taskUserFcn ("T1" , 10000 , zFlag, pFlag, dFlag, gFlag, sFlag, eFlag, nFlag, mFlag, tFlag, cFlag, position, delta, duty1, duty2, vel)
task2 = encoder_task.taskEncoderFcn("T2" , 10000 , zFlag, pFlag, dFlag, gFlag, sFlag, position, delta, vel)
task3 = motor_task.taskMotorFcn("T3", 10000, eFlag, nFlag, mFlag, cFlag, sFlag, tFlag, duty1, duty2 )



tasklist = [task1 , task2, task3]

if __name__  == '__main__':
    
    while True:
    
        try:
            for task in tasklist:
                next(task)
                
        except KeyboardInterrupt:
            break
    
    

    print("Program Terminating")
        
        
        

