'''!@file rtPanel_task.py
    @brief          
    @details        
    
    @author         O'Connell, Joseph
    @author         Osborne, Barrett
    @date           March 18, 2022

'''

import time, RTpanel

def taskRTpanelFcn(taskName, period, zFlag, ballXYZ, calFlag, eFlag, calDATA, bFlag, Kp, Kd, position1, vel, KpOut, KdOut, ballVel, ballPos, Xtranspose):
    '''!@brief          Serves as a medium between user_task.py and resistive touch panel
        @details        Facillitates communication between user_task.py and the risistive touch panel driver,
                        RTpanel.py. This code will recieve signals from user_task and go and get
                        information from the RT panel driver.
                        
        @param taskName         Name of task
        @param period           Period in which this task operates (time)
        @param ballXYZ          Stores XYZ coordinates from the platform
        @param calFlag          Tells this task that calibration needs to occur
        @param eFlag            Tells this task in 'e' has been pressed
        @param calDATA          calibration data
        @param bFlag            Tells this task in 'b' has been pressed
        @param Kp               Stores a porportional gain value
        @param Kd               Stores a derivative gain value
        @param position1        IMU euler angles
        @param vel              IMU angular velocity
        @param KpOut            Stores a porportional gain value
        @param KdOut            Stores a derivative gain value
        @param ballVel          ball velocities
        @param ballPos          ball positions
        @param Xtranspose       Panel data matrix used for panel calibration
    
    '''

    ##  @brief      Keeps track of which state the RT panel task is in
    #   
    state = 1
    
    ##  @brief      next time that task will need to move on
    #   
    next_time = time.ticks_add(time.ticks_us() , period)
    
    ##  @brief      RT panel object
    # 
    my_panel = RTpanel.RTpanel()
    
    my_panel.UpdateBMatrix(Xtranspose.read())

    while True:
            
            current_time = time.ticks_us()
            
            if time.ticks_diff(current_time, next_time) >=0:
                next_time = time.ticks_add(next_time, period)
                
                if state == 1: #init
                    vx = 0
                    vy = 0
                    x0 = 0
                    y0 = 0
                    alpha = 0.85
                    beta = 0.05
                    state = 2
                    start_time = time.ticks_us()
                    
                elif state == 2:
                    Ts = time.ticks_diff(time.ticks_us(), start_time)/1_000_000
                    x1, y1, z1 = my_panel.xyzScanFast()
                
                    if z1 == 1:
                    
                        x0 = x0 + Ts*vx
                        dx = x1 - x0
                        x0 = x0 + alpha*(x1-x0) + Ts*vx
                        vx = vx + (beta/Ts)*dx
                    
                        y0 = y0 + Ts*vy
                        dy = y1 - y0
                        y0 = y0 + alpha*(y1-y0) + Ts*vy
                        vy = vy + (beta/Ts)*dy
                    
                        ballVel.write([vx, vy])
                        ballPos.write([x0, y0])
                        
                    elif z1 == 0:
                        vx = 0
                        vy = 0
                        x0 = 0
                        y0 = 0
                    
                        ballVel.write([0, 0])
                        ballPos.write([0, 0])
                        start_time = time.ticks_us()    
                            
                    else:
                        start_time = time.ticks_us()
                        
                    
                        
                else:
                    raise ValueError(f"invalid state in {taskName}")
                
                next_time = time.ticks_add(next_time , period)
                
                yield state
                
            else:
                yield None