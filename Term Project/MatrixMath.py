

import numpy

Xtranspose = [[-1.03125, 67.58985, -72.31641, -71.97266, 67.28906], [-0.8300781, 29.73633, 30.41992, -33.54492, -31.71387], [1, 1, 1, 1, 1]]

X = numpy.transpose(Xtranspose)

#print('X = ')
#print(X)

Ytranspose = [[0, 80, -80, -80, 80], [0, 40, 40, -40, -40], [1, 1, 1, 1, 1]]

Y = numpy.transpose(Ytranspose)

#print('Y = ')
#print(Y)

XtransX = numpy.dot(Xtranspose, X)
#print(XtransX)

B = numpy.dot(numpy.dot(numpy.linalg.inv(XtransX),Xtranspose),Y)

print('B = ')
print(B)

x = [-53.32422 , 16.77246, 1]

y = numpy.dot(x,B)

print(y)


