'''!@file motor_task.py
    @brief          Creates motor object using DRV8847.py
    @details        Uses DRV8847.py to create a motor controller object (DRV8847).
                    Also creates two motors using DRV8847.py. Facillitates communication
                    between suer_task.py and the physical motors.
    @author         O'Connell, Joseph
    @author         Osborne, Barrett
    @date           March 18, 2022

'''

import time, RTpanel , pyb, motor, closedLoop

def taskMotorFcn(taskName, period, eFlag, nFlag, mFlag, cFlag, sFlag, tFlag, duty1, duty2, ref_roll, ref_pitch, Kp, Kd, wFlag, rFlag, vel, position1, ballXYZ, bFlag, KpOut, KdOut, Xtranspose, ballVel, ballPos):
    '''!@brief          This task is in charge of interfacing with the motors
        @details        Runs controls
                           
        @param taskName         Name of task
        @param period           Period in which this task operates (time)
        @param eFlag            Tells this task in 'e' has been pressed
        @param nFlag            Tells this task in 'n' has been pressed
        @param mFlag            Tells this task in 'm' has been pressed
        @param cFlag            Tells this task in 'c' has been pressed
        @param sFlag            Tells this task in 's' has been pressed
        @param tFlag            Tells this task in 't' has been pressed
        @param duty1            Stores duty cycle of motor 1
        @param duty2            Stores duty cycle of motor 2
        @param ref_roll         Stores a reference roll angle value
        @param ref_pitch        Stores a reference pitch angle value
        @param Kp               Stores a porportional gain value
        @param Kd               Stores a derivative gain value
        @param wFlag            Tells this task in 'w' has been pressed
        @param rFlag            Tells this task in 'r' has been pressed
        @param position1        IMU euler angles
        @param vel              IMU angular velocity
        @param ballXYZ          Stores XYZ coordinates from the platform
        @param bFlag            Tells this task in 'b' has been pressed
        @param KpOut            Stores a porportional gain value
        @param KdOut            Stores a derivative gain value
        @param Xtranspose       Panel data matrix used for panel calibration
        @param ballVel          ball velocities
        @param ballPos          ball positions
       
       
    '''
    ##  @brief      Keeps track of which state the motor task is in
    # 
    state = 1
    
    PWM_tim = pyb.Timer(3, freq = 20_000)
    
    ##  @brief      next time that task will need to move on
    #   
    next_time = time.ticks_add(time.ticks_us() , period) 
    
    ##  @brief      motor object 1
    #   
    motor_1 = motor.Motor(PWM_tim, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2)
    
    ##  @brief      motor object 2
    # 
    motor_2 = motor.Motor(PWM_tim, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4)

    ##  @brief      RT panel object
    # 
    my_panel = RTpanel.RTpanel()
    
    my_panel.UpdateBMatrix(Xtranspose.read())
    
    while True:
        
        ##  @brief      current time of motor_task.py
        # 
        current_time = time.ticks_us()
        
        ballXYZ.write(my_panel.xyzScanFast())
                    
        if time.ticks_diff(current_time, next_time) >=0:
            
            ##  @brief      next time of motor_task.py
            # 
            next_time = time.ticks_add(next_time, period)
                
            if state == 1:    
                
                if wFlag.read():
                    
                    ##  @brief      controller object (roll)
                    # 
                    controllerRoll = closedLoop.ClosedLoop(ref_roll.read())
                    controllerRoll.setKp(Kp.read())
                    controllerRoll.setKd(Kd.read())
                    
                    ##  @brief      controller object (pitch)
                    # 
                    controllerPitch = closedLoop.ClosedLoop(ref_pitch.read())
                    controllerPitch.setKp(Kp.read())
                    controllerPitch.setKd(Kd.read())
                    
                    rollAngle, pitchAngle, headAngle = position1.read()
                    rollVel, pitchVel, headVel = vel.read()
                    
                    motor_2.set_duty(controllerRoll.pdCont(rollAngle, -1*rollVel,0))
                    motor_1.set_duty(controllerPitch.pdCont(pitchAngle, -1*pitchVel,0))
                
                elif bFlag.read() == True:
                    vx, vy = ballVel.read()
                    x0, y0 = ballPos.read()
                    
                    ##  @brief      controller object (Ball X position)
                    # 
                    controllerX = closedLoop.ClosedLoop(0)
                    controllerX.setKp(KpOut.read())
                    controllerX.setKd(KdOut.read())
                    
                    ##  @brief      controller object (Ball Y position)
                    # 
                    controllerY = closedLoop.ClosedLoop(0)
                    controllerY.setKp(KpOut.read())
                    controllerY.setKd(KdOut.read())
                    
                    ##  @brief      reference pitch angle
                    # 
                    thetaRefx = (-1*(controllerX.pdCont(x0, vx, 0)))    #Motor1
                    
                    ##  @brief      reference roll angle
                    # 
                    thetaRefy = (controllerY.pdCont(y0, vy, 0))         #Motor2
                    
                    ##  @brief      controller object (roll)
                    #
                    controllerRoll = closedLoop.ClosedLoop(thetaRefy)
                    controllerRoll.setKp(Kp.read())
                    controllerRoll.setKd(Kd.read())
                
                    ##  @brief      controller object (pitch)
                    # 
                    controllerPitch = closedLoop.ClosedLoop(thetaRefx)
                    controllerPitch.setKp(Kp.read())
                    controllerPitch.setKd(Kd.read())
                    
                    rollAngle, pitchAngle, headAngle = position1.read()
                    rollVel, pitchVel, headVel = vel.read()
                    
                    ##  @brief      saturation limit; maximum value of duty cycle
                    # 
                    satLimitMax =  40
                    
                    ##  @brief      saturation limit; minimum value of duty cycle
                    # 
                    satLimitMin = -40
                    
                    if controllerRoll.pdCont(rollAngle, -1*rollVel,0) > satLimitMax:
                        motor_2.set_duty(satLimitMax)
                    elif controllerRoll.pdCont(rollAngle, -1*rollVel,0) < satLimitMin:
                        motor_2.set_duty(satLimitMin)
                    elif controllerPitch.pdCont(pitchAngle, -1*pitchVel,0) > satLimitMax:
                        motor_1.set_duty(satLimitMax)
                    elif controllerPitch.pdCont(pitchAngle, -1*pitchVel,0) < satLimitMin:
                        motor_1.set_duty(satLimitMin)
                    else:
                        motor_2.set_duty(controllerRoll.pdCont(rollAngle, -1*rollVel,0))
                        motor_1.set_duty(controllerPitch.pdCont(pitchAngle, -1*pitchVel,0))

#                    print(controllerRoll.pdCont(rollAngle, -1*rollVel,0))
#                    print(controllerPitch.pdCont(pitchAngle, -1*pitchVel,0))
#                        time.sleep(0.05)
                else:
                    motor_1.set_duty(0)
                    motor_2.set_duty(0)
                    
                
            else:
                raise ValueError(f"invalid state in {taskName}")
            
            next_time = time.ticks_add(next_time , period)
            
            yield state
            
        else:
            yield None
        