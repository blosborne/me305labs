'''!@file BNO055.py
    @brief          Creates BNO055 (an IMU) object
    @details        Creates a BNO055 object. This is a motor controller. This has the following functions;
                        change operating mode
                        retrieve calibration status
                        retrieve calibration coefficients
                        wrtie calibration coefficients back to IMU from pre-recorded data
                        read Euler angles
                        read angular velocity
                        
    @author         O'Connell, Joseph
    @author         Osborne, Barrett
    @date           March 18, 2022

'''
import struct, time

class BNO055:
    '''!@brief      A IMU driver class for the BNO055 from BOSCH.
        @details    Objects of this class can be used to configure the BNO055
                    IMU and retrieve data from it.
    
                    Refer to the BNO055 datasheet here:
                    https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf
    '''
    
    def __init__ (self, i2cName):
        '''!@brief Initializes and returns a DRV8847 object.
        
            @param i2cName  name of I2C object

        '''
        self.I2C = i2cName
        self.I2C.mem_write(0b00000000, 0x28, 0x3B) #sets to degree measurement
        
        self.p0 = 0
        self.p1 = 0
        self.bufcal = bytearray(22*[0])
        
        pass
    
    def configMode (self):
        '''!@brief 
        '''
        self.I2C.mem_write(0b00000000, 0x28, 0x3D)
        time.sleep(0.007)
        pass
    
    def imuMode (self):
        '''!@brief 
        '''
        self.I2C.mem_write(0b00001000, 0x28, 0x3D)
        pass
    
    def calStat (self):
        '''!@brief 
        '''
        cal_byte = self.I2C.mem_read(1, 0x28, 0x35)[0]

        self.mag_stat = cal_byte & 0b00000011
        self.acc_stat = (cal_byte & 0b00001100)>>2
        self.gyr_stat = (cal_byte & 0b00110000)>>4
        self.sys_stat = (cal_byte & 0b11000000)>>6 
        return (self.mag_stat, self.acc_stat, self.gyr_stat, self.sys_stat)

    
    def calCoefRead (self):
        '''!@brief Brings the DRV8847 out of sleep mode.
        '''
        self.I2C.mem_read(self.bufcal, 0x28, 0x55)
        return self.bufcal
    
    def calCoefWrite (self, data):
        '''!@brief Brings the DRV8847 out of sleep mode.
        
            @param data     calibration data
        '''   
        self.I2C.mem_write(data, 0x28, 0x55)
        pass
    
    def eulerAng (self):
        '''!@brief Brings the DRV8847 out of sleep mode.
        '''
        buf = bytearray([0,0,0,0,0,0])
        self.I2C.mem_read(buf, 0x28, 0x1A)
        (headAngle, rollAngle, pitchAngle) = struct.unpack('<hhh', buf)

        return (rollAngle/16, pitchAngle/16, headAngle/16)
    
    def angVel (self):
        '''!@brief Brings the DRV8847 out of sleep mode.
        '''
        buf = bytearray([0,0,0,0,0,0])
        self.I2C.mem_read(buf, 0x28, 0x14)       
        (headVel, rollVel, pitchVel) = struct.unpack('<hhh', buf)
        
        return (rollVel/16, pitchVel/16, headVel/16)
    
#if __name__ == '__main__':

#    #my_i2c = pyb.i2c.init(pyb.I2C.CONTROLLER)
#    my_i2c = I2C(1, I2C.CONTROLLER)
#    BNO = BNO055(my_i2c)
#    
#    BNO.configMode()
#    BNO.imuMode()
    
    
