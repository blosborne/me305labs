'''!    @file closedLoop.py
        @brief                                  Acts as a proportional controller for a given input and reference value
        @details                                Takes in a value and a reference value. Computes the difference between the two.
                                                This difference is multiplied by a user supplied gain.
                                                
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   February 10, 2022
'''

class ClosedLoop:
    '''!@brief                                  Acts as a proportional controller for a given input and reference value
        @details                                Takes in a value and a reference value. Computes the difference between the two.
                                                This difference is multiplied by a user supplied gain.
    '''
    def __init__(self, reference_value):
        '''!@brief                              Defines reference value for propportional controller
        
            @param reference_value              reference value used in closed loop controller
        '''
        
        self.reference = reference_value
        
        pass
 
    def setKp(self, kProp):
        '''!@brief                              Defines proportional controller constant
        
            @param kProp                        proportional controller constant
        '''
        
        self.gainP = kProp
        
        pass
    
    def setKd(self, kPd):
        '''!@brief                              Defines derivative controller constant
        '''
        
        self.gainPd = kPd
        
        pass
       
    def propCont(self, measured_value):
        '''!@brief                              Acts as a proportional controller for a given input and reference value
            @details                            Takes in a value and a reference value. Computes the difference between the two.
                                                This difference is multiplied by a user supplied gain.
                                                
            @param measured_value               measured value used in closed loop controller
        '''
        
        self.output = self.gainP*(self.reference - measured_value)
        
        return self.output
    
    def pdCont(self, measured_value, measured_value_dot, reference_dot):
        '''!@brief                              Acts as a pd controller for a given input and reference value
            @details                            Takes in a value and a reference value. Computes the difference between the two.
                                                This difference is multiplied by a user supplied gain.
                                                
            @param measured_value               measured value used in closed loop controller
            @param measured_value_dot           derivative of measured value used in closed loop controller
            @param reference_dot                reference value for derivative
        '''
        
        pdOut = self.gainP*(self.reference - measured_value) + self.gainPd*(reference_dot - measured_value_dot)
        
        return pdOut
    

