'''!    @file user_task.py
        @brief                                  Generates and manages a user interface for the encoder
        @details        The user interface prints the following help menu;

                                                                +----------------------------------------------------+
                                                                |                    Main Menu                       |
                                                                |                   Created by:                      |
                                                                |         Barrett Osborne, Joseph OConnell           |
                                                                +----------------------------------------------------+
                                                                | (z or Z)   Print touch panel position              |
                                                                | (p or P)   Print platform angles                   |              
                                                                | (v or V)   Print platform angular velocities       |
                                                                | (l or L)   Print calibration instructions          |
                                                                | (g or G)   Begin or check touch panel calibration  |
                                                                | (c or C)   Begin or check calibration status       |
                                                                | (q or Q)   Print calibration coefficients          |
                                                                | (e or E)   Enable BNO055                           |
                                                                | (b or B)   Enable or disable ball balancing        |
                                                                | (w or W)   Enable or disable closed Loop control   |
                                                                | (r or R)   Set reference platform angles           |
                                                                | (k or K)   Set gain values                         |
                                                                | (d or D)   Collect Ball Balance Data               |
                                                                | (h or H)   Display this help message               |
                                                                +----------------------------------------------------+
                                                                          
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   March 18, 2022
'''

import time , pyb

def taskUserFcn (taskName , period , ref_pitch, ref_roll, Kd, calFlag, pFlag, eFlag, cFlag, position1, duty1, duty2, vel, calStatReturn, wFlag, Kp, zFlag, ballXYZ, bFlag, gFlag, KpOut, KdOut, Xtranspose, dFlag):
    '''!@brief          Generates and manages a user interface for the encoder
        @details        The user interface prints the following help menu;

                                                                +----------------------------------------------------+
                                                                |                    Main Menu                       |
                                                                |                   Created by:                      |
                                                                |         Barrett Osborne, Joseph OConnell           |
                                                                +----------------------------------------------------+
                                                                | (z or Z)   Print touch panel position              |
                                                                | (p or P)   Print platform angles                   |              
                                                                | (v or V)   Print platform angular velocities       |
                                                                | (l or L)   Print calibration instructions          |
                                                                | (g or G)   Begin or check touch panel calibration  |
                                                                | (c or C)   Begin or check calibration status       |
                                                                | (q or Q)   Print calibration coefficients          |
                                                                | (e or E)   Enable BNO055                           |
                                                                | (b or B)   Enable or disable ball balancing        |
                                                                | (w or W)   Enable or disable closed Loop control   |
                                                                | (r or R)   Set reference platform angles           |
                                                                | (k or K)   Set gain values                         |
                                                                | (d or D)   Collect Ball Balance Data               |
                                                                | (h or H)   Display this help message               |
                                                                +----------------------------------------------------+
                                                               
                        The user interface prints the following calibration menu;
                       
                                                                +---------------------------------------------------------------+
                                                                |                     Platform Calibration:                     |
                                                                +---------------------------------------------------------------+
                                                                | To calibrate perform the following:                           |
                                                                | Place the device in 6 different stable positions for a        |
                                                                | period of few seconds to allow the accelerometer to calibrate.|
                                                                |                             NOTES                             |
                                                                | Make sure that there is slow movement between the             |
                                                                | 2 stable positions                                            |
                                                                | The 6 stable positions could be in any direction              |
                                                                | During calibration the status is printed until calibrated     |
                                                                +---------------------------------------------------------------+
                                                                |              When ready to calibrate press c or C             |
                                                                +---------------------------------------------------------------+
                                                                                                                                 
                                                                +---------------------------------------------------------------+
                                                                |                  Touch Panel Calibration:                     |
                                                                +---------------------------------------------------------------+
                                                                | To calibrate perform the following:                           |
                                                                | Using a small object with a fine tip such as a screwdriver    |
                                                                | touch the center of the touch panel, then touch each          |
                                                                | quadrents far corners as shown with an O in the notes below   |
                                                                |                             NOTES                             |
                                                                |    O-------------+--------------+-------------+-------------O |
                                                                |    |             |              |             |             | |
                                                                |    |    SECOND   |              |             |    FIRST    | |
                                                                |    |             |              |             |             | |
                                                                |    +-------------+--------------O-------------+-------------| |
                                                                |    |             |              |             |             | |
                                                                |    |    THIRD    |              |             |   FOURTH    | |
                                                                |    |             |              |             |             | |
                                                                | Y  O-------------+--------------+-------------+-------------O |
                                                                | |_ X                                                          |
                                                                |                                                               |
                                                                | After each poke the calibration coefficients for each         |
                                                                | location will print, once each location is calibrated all     |
                                                                | calibration coefficients will be printed.                     |
                                                                +---------------------------------------------------------------+
                                                                |              When ready to calibrate press g or G             |
                                                                +---------------------------------------------------------------+
                                                                       
        @param taskName         Name of task
        @param period           Period in which this task operates (time)
        @param ref_roll         Stores a reference roll angle value
        @param ref_pitch        Stores a reference pitch angle value
        @param Kd               Stores a derivative gain value
        @param calFlag          Tells tasks that calibration needs to occur
        @param pFlag            Tells tasks 'p' has been pressed
        @param pFlag            Tells tasks 'd' has been pressed
        @param eFlag            Tells tasks 'e' has been pressed
        @param cFlag            Tells tasks 'c' has been pressed
        @param position1        IMU euler angles
        @param duty1            Stores duty cycle of motor 1
        @param duty2            Stores duty cycle of motor 2
        @param vel              IMU angular velocity
        @param calStatReturn    IMU calibration status
        @param wFlag            Tells tasks 'w' has been pressed
        @param Kp               Stores a porportional gain value
        @param wFlag            Tells tasks 'w' has been pressed
        @param ballXYZ          Stores XYZ coordinates from the platform
        @param bFlag            Tells tasks 'b' has been pressed
        @param gFlag            Tells tasks 'g' has been pressed
        @param KpOut            Stores a porportional gain value
        @param KdOut            Stores a derivative gain value
        @param Xtranspose       Panel data matrix used for panel calibration
        @param dFlag            Tells tasks 'd' has been pressed

    '''
    ##  @brief      Keeps track of which state the user task is in
    #   
    state = 0
    ##  @brief      next time code should run through primary if statement
    # 
    next_time = time.ticks_add(time.ticks_us() , period)

    ##  @brief      virtual com port
    # 
    ser = pyb.USB_VCP()
        
    while True:
        
        ##  @brief      Current time of the system
        #   
        current_time = time.ticks_us()
        
        if time.ticks_diff(current_time, next_time) >=0:
            
            next_time = time.ticks_add(next_time, period)
            
            if state == 0:
                printhelp()
                if cFlag.read() == True:
                    print('+--------------IMU calibration required--------------+')
                    print('|         To begin calibration press c or C          |')
                    print('|     For calibration instructions press l or L      |')
                    print("+----------------------------------------------------+")
                    cFlag.write(False)
                    state = 1
                else:
                    state = 1
                
                
            elif state == 1:
                if ser.any():
                    ##  @brief      Most recent character inputted to keyboard
                    # 
                    charIn = ser.read(1).decode()
                    
                    if charIn in {'p' , 'P'}:
                        pFlag.write(True)
                        print('+-------Euler Angles-------+')
                        print('Roll, Pitch, Heading')
                        rollAngle, headAngle, pitchAngle = position1.read()
                        print(rollAngle, headAngle, pitchAngle)
                        state = 1
                        
                    elif charIn in {'d' , 'D'}:
                        dFlag.write(True)
                        state = 1
                        
                    elif charIn in {'h' , 'H'}:
                        printhelp() 
                        state = 1
                    
                    elif charIn in {'j' , 'J'}:
                        print('Enter KpOut (0.15) FOR BALL POSITION [%]')
                        bufKpOut = ''
                        bufKdOut = ''
                        order = 0
                        state = 5  
                    elif charIn in {'z' , 'Z'}:
                        x , y , z = ballXYZ.read()
                        
                        if z == 1:
#                           '+-------Ball Position-------+'
#                           'X position, Y position'
                            print(x, y)
                        else:
                            print('No contact detected')
                    
                    elif charIn in {'e' , 'E'}:
                        eFlag.write(True)
                        print("Enabling BNO055.")
                        state = 1
                            
                    elif charIn in {'v' , 'V'}:
                        print('+----Angular Velocities----+')
                        print('\rRoll, Pitch, Heading')
                        print(vel.read())
                        state = 1
                        
                    elif charIn in {'l', 'L'}:
                        printCalInt()
                        state = 1
                    
                    elif charIn in {'k' , 'K'}:
                        print('Enter Kp (3.8) [%]')
                        
                        ## @brief buffer to hold Kp input from user
                        #
                        bufKp = ''
                        ## @brief buffer to hold Kd input from user
                        #
                        bufKd = ''
                        ##  @brief      Keeps track of which sub state the state 4 is in
                        #  
                        order = 0
                        state = 4  
                        
                    elif charIn in {'q' , 'Q'}:
                        magCal, accelCal, gyroCal, sysCal = calStatReturn.read()
                        print(f'Magnitometer: {magCal}, Accelerometer: {accelCal}, Gyroscope: {gyroCal}, System: {sysCal}') 
                        state = 1
                        
                    elif charIn in {'w' , 'W'}:
                        if wFlag.read() == False:
                            wFlag.write(True)
                            print("Enabling closed loop control.")

                        elif wFlag.read() == True:
                            wFlag.write(False)
                            print("Disabling closed loop control.")
                        state = 1  
                    
                    elif charIn in {'b' , 'B'}:
                        if bFlag.read() == False:
                            bFlag.write(True)
                            print("Enabling ball balance.")

                        elif bFlag.read() == True:
                            bFlag.write(False)
                            print("Disabling ball balance.")
                        state = 1 
                            
                    elif charIn in {'r' , 'R'}:
                        print('Enter roll angle [deg]')
                        
                        ## @brief buffer to hold roll angle input from user
                        #
                        bufK = ''
                        ## @brief buffer to hold pitch angle input from user
                        #
                        bufY = ''
                        order = 0
                        
                        state = 3    
                        
                    elif charIn in {'c' , 'C'}:
                        
                        magCal, accelCal, gyroCal, sysCal = calStatReturn.read()
                            
                        if accelCal == 3 and gyroCal == 3:
                            print(calStatReturn.read())  
                            print("No calibration needed!")
                            
                            state = 1
                        else:
                            print('Beginning calibration')
                            state = 2
                            
                    elif charIn in {'g' , 'G'}:
                        
                        gFlag.write(True)
                        print('Beginning Calibration:')
                        print('Poke the center of the panel')
                        
                        ##  @brief      x collected during calibration
                        # 
                        x = [0,0,0,0,0]
                        ##  @brief      y collected during calibration
                        # 
                        y = [0,0,0,0,0]
                     
                        ##  @brief      used to iterate
                        #   @details    Used in the iteration needed to change the values of the indexes
                        #               of posdata and timedata
                        # 
                        i = 0
                        
                        state = 6
                   
                    else:
                        print(f"Input {charIn} not allowed")
                        
            
            elif state == 2: #Prints calibration status until calibrated
                
                magCal, accelCal, gyroCal, sysCal = calStatReturn.read()
                    
                if accelCal == 3 and gyroCal == 3:
                    print(calStatReturn.read())
                    print("Calibration is complete!")
                            
                    state = 1
                else:
                    print(calStatReturn.read())
                
                            
            elif state == 3:       #set ref angles

                if order == 0:
                    if ser.any(): #check if char is inputed
                        charDutyK = ser.read(1).decode() 
                        
                        if charDutyK.isdigit(): #What to do if the char is a char
                            bufK += charDutyK 
                            ser.write(charDutyK)
                            
                        elif charDutyK == '-': #if it is decimal 
                            if len(bufK) == 0:
                                bufK += charDutyK
                                ser.write(charDutyK)
                                
                            else:
                                state = 3
                        
                        elif charDutyK == '.': #if it is decimal 
                            if '.' in bufK:
                                state = 3
                                
                            else:
                                bufK += charDutyK
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufK) == 0:
                                state = 3
                                
                            else:
                                bufK = bufK[:-1]
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufK) == 0:
                                print('Roll Angle has not changed')
                                print('Enter pitch angle [deg]')
                                order = 1
                            
                            elif float(bufK) >= -15 and float(bufK) <= 15:
                                ref_roll.write(float(bufK))
                                print(f'\rRoll Angle set to {bufK} deg')
                                print('Enter pitch angle [deg]')
                                order = 1
                                                    
                            else:
                                print('\rInvalid Roll Angle')
                                bufK = ''
                                order = 0
                    
                elif order == 1:
                    
                    if ser.any(): #check if char is inputed
                        charDutyY = ser.read(1).decode() 
                        
                        if charDutyY.isdigit(): #What to do if the char is a char
                            bufY += charDutyY 
                            ser.write(charDutyY)
                            
                        elif charDutyY == '-': #if it is minus
                            if len(bufY) == 0:
                                bufY += charDutyY
                                ser.write(charDutyY)
                                
                            else:
                                order = 1
                        
                        elif charDutyY == '.': #if it is decimal 
                            if '.' in bufY:
                                order = 1
                                
                            else:
                                bufY += charDutyY
                                ser.write(charDutyY)
                            
                        elif charDutyY in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufY) == 0:
                                state = 1
                            
                            else:
                                bufY = bufY[:-1]
                                ser.write(charDutyY)
                            
                        elif charDutyY in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufY) == 0:
                                print('Pitch angle has not changed')
                                state = 1
                            
                            elif float(bufY) >= -15 and float(bufY) <= 15:
                                ref_pitch.write(float(bufY))
                                print(f'\rPitch angle set to {bufY} deg')
                                state = 1
                                                    
                            else:
                                print('\rInvalid pitch angle')
                                bufY = ''
                                order = 1
                    
            elif state == 4:       

                if order == 0:
                    #run k states code
                    if ser.any(): #check if char is inputed
                        charDutyK = ser.read(1).decode() 
                        
                        if charDutyK.isdigit(): #What to do if the char is a char
                            bufKp += charDutyK 
                            ser.write(charDutyK)
                            
                        elif charDutyK == '.': #if it is decimal 
                            if '.' in bufKp:
                                state = 4
                                
                            else:
                                bufKp += charDutyK
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufKp) == 0:
                                state = 4
                                
                            else:
                                bufKp = bufKp[:-1]
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufKp) == 0:
                                print('Kp has not changed')
                                print('Enter Kd (.15) [%]')
                                order = 1
                            
                            elif float(bufKp) >= 0 and float(bufKp) <= 100:
                                Kp.write(float(bufKp))
                                print(f'\rKp set to {bufKp}%')
                                print('Enter Kd (.15) [%]')
                                order = 1
                                                    
                            else:
                                print('\rInvalid Kp')
                                bufKp = ''
                                order = 0
                    
                elif order == 1:
                    
                    if ser.any(): #check if char is inputed
                        charDutyY = ser.read(1).decode() 
                        
                        if charDutyY.isdigit(): #What to do if the char is a char
                            bufKd += charDutyY 
                            ser.write(charDutyY)
                            
                        elif charDutyY == '.': #if it is decimal 
                            if '.' in bufKd:
                                order = 1
                                
                            else:
                                bufKd += charDutyY
                                ser.write(charDutyY)
                            
                        elif charDutyY in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufKd) == 0:
                                order = 1
                            
                            else:
                                bufKd = bufKd[:-1]
                                ser.write(charDutyY)
                            
                        elif charDutyY in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufKd) == 0:
                                print('Kd has not changed')
                                state = 1
                            
                            elif float(bufKd) >= 0 and float(bufKd) <= 1:
                                Kd.write(float(bufKd))
                                print(f'\rKD set to {bufKd}%')
                                state = 1
                                                    
                            else:
                                print('\rInvalid Kd')
                                bufKd = ''
                                order = 1
                                
            elif state == 5:   #second Kp state    

                if order == 0:
                    #run k states code
                    if ser.any(): #check if char is inputed
                        charDutyK = ser.read(1).decode() 
                        
                        if charDutyK.isdigit(): #What to do if the char is a char
                            bufKpOut += charDutyK 
                            ser.write(charDutyK)
                            
                        elif charDutyK == '.': #if it is decimal 
                            if '.' in bufKpOut:
                                state = 5
                                
                            else:
                                bufKpOut += charDutyK
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufKpOut) == 0:
                                state = 5
                                
                            else:
                                bufKpOut = bufKpOut[:-1]
                                ser.write(charDutyK)
                            
                        elif charDutyK in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufKpOut) == 0:
                                print('KpOut has not changed')
                                print('Enter KdOut [%]')
                                order = 1
                            
                            elif float(bufKpOut) >= 0 and float(bufKpOut) <= 10:
                                KpOut.write(float(bufKpOut))
                                print(f'\rKpOut set to {bufKpOut}%')
                                print('Enter KdOut [%]')
                                order = 1
                                                    
                            else:
                                print('\rInvalid KpOut')
                                bufKpOut = ''
                                order = 0
                    
                elif order == 1:
                    
                    if ser.any(): #check if char is inputed
                        charDutyY = ser.read(1).decode() 
                        
                        if charDutyY.isdigit(): #What to do if the char is a char
                            bufKdOut += charDutyY 
                            ser.write(charDutyY)
                            
                        elif charDutyY == '.': #if it is decimal 
                            if '.' in bufKdOut:
                                order = 1
                                
                            else:
                                bufKdOut += charDutyY
                                ser.write(charDutyY)
                            
                        elif charDutyY in {'\b','\x08', '\x7F'}: #If backspace is pressed
                            if len(bufKdOut) == 0:
                                order = 1
                            
                            else:
                                bufKdOut = bufKdOut[:-1]
                                ser.write(charDutyY)
                            
                        elif charDutyY in {'\r','\n'}: #If enter is pressed
                            
                            if len(bufKdOut) == 0:
                                print('KdOut has not changed')
                                state = 1
                            
                            elif float(bufKdOut) >= 0 and float(bufKdOut) <= 10:
                                KdOut.write(float(bufKdOut))
                                print(f'\rKdOut set to {bufKdOut}%')
                                state = 1
                                                    
                            else:
                                print('\rInvalid KdOut')
                                bufKdOut = ''
                                order = 1
                    
            elif state == 6:

                xPoke , yPoke , z = ballXYZ.read()
                if i == 0:
                    if z == 1:
                        x[i] = xPoke
                        y[i] = yPoke
                        time.sleep(0.5)
                        print(f'Center: {xPoke}, {yPoke}')
                        print('Poke the first quadrants far corner (+x,+y)')
                        i = i + 1
                elif i == 1:
                    if z == 1:
                        x[i] = xPoke
                        y[i] = yPoke
                        time.sleep(0.5)
                        print(f'First quadrant: {xPoke}, {yPoke}')
                        print('Poke the second quadrants far corner (-x,+y)')
                        i = i + 1
                elif i == 2:
                    if z == 1:
                        x[i] = xPoke
                        y[i] = yPoke
                        time.sleep(0.5)
                        print(f'Second quadrant: {xPoke}, {yPoke}')
                        print('Poke the third quadrants far corner (-x,-y)')
                        i = i + 1
                elif i == 3:
                    if z == 1:
                        x[i] = xPoke
                        y[i] = yPoke
                        time.sleep(0.5)
                        print(f'Third quadrant: {xPoke}, {yPoke}')
                        print('Poke the fourth quadrants far corner (+x,-y)')
                        i = i + 1
                elif i == 4:
                    if z == 1:
                        x[i] = xPoke
                        y[i] = yPoke
                        time.sleep(0.5)
                        print(f'Fourth quadrant: {xPoke}, {yPoke}')
                        i = i + 1
                else:
                    print("Calibration Complete")
                    z = [1,1,1,1,1]
                    panelY = [x,y,z]
                    #panelY = numpy.transpose(panelY)
                    print(panelY)
                    Xtranspose.write(panelY)
                    state = 1
                    
            else:
                
                raise ValueError(f"invalid state in {taskName}")
            
            next_time = time.ticks_add(next_time , period)
            
            yield state
            
        else:
            yield None
                
def printCalInt():
    '''!@brief      Prints the user interface menu for calibration
        @details    A series of print states that forms the calibration user interface menu
    '''
    

    print("+---------------------------------------------------------------+")
    print("|                     Platform Calibration:                     |")
    print("+---------------------------------------------------------------+")
    print("| To calibrate perform the following:                           |")
    print("| Place the device in 6 different stable positions for a        |")
    print("| period of few seconds to allow the accelerometer to calibrate.|")
    print("|                             NOTES                             |")
    print("| Make sure that there is slow movement between the             |")
    print("| 2 stable positions                                            |")
    print("| The 6 stable positions could be in any direction              |")
    print("| During calibration the status is printed until calibrated     |")
    print("+---------------------------------------------------------------+")
    print("|              When ready to calibrate press c or C             |")
    print("+---------------------------------------------------------------+")
    print("                                                                 ")
    print("+---------------------------------------------------------------+")
    print("|                  Touch Panel Calibration:                     |")
    print("+---------------------------------------------------------------+")
    print("| To calibrate perform the following:                           |")
    print("| Using a small object with a fine tip such as a screwdriver    |")
    print("| touch the center of the touch panel, then touch each          |")
    print("| quadrents far corners as shown with an O in the notes below   |")
    print("|                             NOTES                             |")
    print("|    O-------------+--------------+-------------+-------------O |")
    print("|    |             |              |             |             | |")
    print("|    |    SECOND   |              |             |    FIRST    | |")
    print("|    |             |              |             |             | |")
    print("|    +-------------+--------------O-------------+-------------| |")
    print("|    |             |              |             |             | |")
    print("|    |    THIRD    |              |             |   FOURTH    | |")
    print("|    |             |              |             |             | |")
    print("| Y  O-------------+--------------+-------------+-------------O |")
    print("| |_ X                                                          |")
    print("|                                                               |")
    print("| After each poke the calibration coefficients for each         |")
    print("| location will print, once each location is calibrated all     |")
    print("| calibration coefficients will be printed.                     |")
    print("+---------------------------------------------------------------+")
    print("|              When ready to calibrate press g or G             |")
    print("+---------------------------------------------------------------+")
    
def printhelp():
    '''!@brief      Prints the user interface menu
        @details    A series of print states that forms the user interface menu
    '''
    
    print("+----------------------------------------------------+")
    print("|                    Main Menu                       |")
    print("|                   Created by:                      |")
    print("|         Barrett Osborne, Joseph OConnell           |")
    print("+----------------------------------------------------+")
    print("| (z or Z)   Print touch panel position              |")
    print("| (p or P)   Print platform angles                   |")              
    print("| (v or V)   Print platform angular velocities       |")
    print("| (l or L)   Print calibration instructions          |")
    print("| (g or G)   Begin or check touch panel calibration  |")
    print("| (c or C)   Begin or check calibration status       |")
    print("| (q or Q)   Print calibration coefficients          |")
    print("| (e or E)   Enable BNO055                           |")
    print("| (b or B)   Enable or disable ball balancing        |")
    print("| (w or W)   Enable or disable closed Loop control   |")
    print("| (r or R)   Set reference platform angles           |")
    print("| (k or K)   Set gain values                         |")
    print("| (d or D)   Collect Ball Balance Data               |")
    print("| (h or H)   Display this help message               |")
    print("+----------------------------------------------------+")