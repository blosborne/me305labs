'''!@file RTpanel.py
    @brief          Creates a resistive touch panel object
    @details        Creates a resistive touch panel This has the following functions;
                        retrieve X position
                        retrieve Y position
                        retrieve Z position
                        returns a tuple of the X, Y, Z
                        
    @author         O'Connell, Joseph
    @author         Osborne, Barrett
    @date           March 18, 2022

'''
from pyb import Pin, ADC
from ulab import numpy
#import time

class RTpanel:
    '''!@brief      A resistive touch panel driver class.
        @details    Objects of this class can be used to configure a resistive touch panel
                    and recieve/interpret data from it.
    '''
    
    def __init__ (self):
        '''!@brief Initializes and returns a resistive panel object.

        '''
        #m = minus
        #p = plus
        self.ymPin = Pin(Pin.cpu.A0)
        self.xmPin = Pin(Pin.cpu.A1)
        self.ypPin = Pin(Pin.cpu.A6)
        self.xpPin = Pin(Pin.cpu.A7)
        
        pass
    
    def UpdateBMatrix (self, Xtranspose):
        '''!@brief takes in calibration data matrix and calculates a calibration matrix.
        
            @param Xtranspose   Data collected during calibration of the touch panel
        '''
        Xt = numpy.array(Xtranspose)
        X = Xt.transpose()
        
        Ytranspose = numpy.array([[0, 80, -80, -80, 80], [0, 40, 40, -40, -40], [1, 1, 1, 1, 1]])
        
        Y = Ytranspose.transpose()

        XtransX = numpy.dot(Xt, X)
        
        self.B = numpy.dot(numpy.dot(numpy.linalg.inv(XtransX),Xt),Y)
        
        pass
    
    def xScan (self):
        '''!@brief Retrieves positional data (X) from the RT panel.
        '''
        self.xpPin = Pin(Pin.cpu.A7, Pin.OUT_PP)
        self.xmPin = Pin(Pin.cpu.A1, Pin.OUT_PP)
        self.ypPin = Pin(Pin.cpu.A6, Pin.IN)
        self.ymPin = ADC(Pin(Pin.cpu.A0))
        self.xmPin.low()
        self.xpPin.high()

        adc = self.ymPin
        ym = adc.read()
        xOutRaw = ((ym/4096)*176)-88
        self.xOut = self.B[0][0]*xOutRaw + self.B[2][0]
        return self.xOut
    
    def yScan (self):
        '''!@brief Retrieves positional data (Y) from the RT panel.
        '''
        self.xpPin = Pin(Pin.cpu.A7, Pin.IN)
        self.xmPin = ADC(Pin(Pin.cpu.A1))
        self.ypPin = Pin(Pin.cpu.A6, Pin.OUT_PP)
        self.ymPin = Pin(Pin.cpu.A0, Pin.OUT_PP)
        self.ypPin.high()
        self.ymPin.low()
        adc = self.xmPin
        xm = adc.read()
        
        yOutRaw = ((xm/4096)*100)-50
        self.yOut = self.B[1][1]*yOutRaw + self.B[2][1]
        return self.yOut
        
    def contact (self):
        '''!@brief Retrieves contact data (Z) from the RT panel.
        '''
        self.xpPin = Pin(Pin.cpu.A7, Pin.IN)
        self.xmPin = Pin(Pin.cpu.A1, Pin.OUT_PP)
        self.ypPin = Pin(Pin.cpu.A6, Pin.OUT_PP)
        self.ymPin = ADC(Pin(Pin.cpu.A0))
        self.ypPin.high()
        self.xmPin.low()
        adc = self.ymPin
        xp = adc.read()
        
        if xp < 4000:
            contact = 1
        else:
            contact = 0
        
        return contact
    
    def xyzScan (self):
        '''!@brief Retrieves positional data (X, Y, Z) from the RT panel.
        '''
        # yvector = [(B11*x + B31), (B22*y + B32), z]
        
        self.xpPin = Pin(Pin.cpu.A7, Pin.OUT_PP)
        self.xmPin = Pin(Pin.cpu.A1, Pin.OUT_PP)
        self.ypPin = Pin(Pin.cpu.A6, Pin.IN)
        self.ymPin = ADC(Pin(Pin.cpu.A0))
        self.xmPin.low()
        self.xpPin.high()
        adc = self.ymPin
        ym = adc.read()
        xOutRaw = ((ym/4096)*176)-88
        xOut = self.B[0][0]*xOutRaw + self.B[2][0]    # may need to change the hard coded values
                                        # get from B matrix
                                        # x = (B11*x + B31)
        
        self.xpPin = Pin(Pin.cpu.A7, Pin.IN)
        self.xmPin = ADC(Pin(Pin.cpu.A1))
        self.ypPin = Pin(Pin.cpu.A6, Pin.OUT_PP)
        self.ymPin = Pin(Pin.cpu.A0, Pin.OUT_PP)
        self.ypPin.high()
        self.ymPin.low()
        adc = self.xmPin
        xm = adc.read()
        yOutRaw = ((xm/4096)*100)-50
        yOut = self.B[1][1]*yOutRaw + self.B[2][1]    # may need to change the hard coded values
                                        # get from B matrix
                                        # y = (B22*y + B32)
        
        self.xpPin = Pin(Pin.cpu.A7, Pin.IN)
        self.xmPin = Pin(Pin.cpu.A1, Pin.OUT_PP)
        self.ypPin = Pin(Pin.cpu.A6, Pin.OUT_PP)
        self.ymPin = ADC(Pin(Pin.cpu.A0))
        self.ypPin.high()
        self.xmPin.low()
        adc = self.ymPin
        xp = adc.read()
        
        if xp < 4000:
            contact = 1
        else:
            contact = 0

        # To recalibrate change to commented out return statement
        # and then calculate B matrix using MatrixMath.py
        # and then replace hard coded values in xOut and yOut
        
        #return xOutRaw, yOutRaw, contact
        return xOut, yOut, contact
    
    def xyzScanFast (self): #still not very fast
        '''!@brief Retrieves positional data (X, Y, Z) from the RT panel.
        '''
        # yvector = [(B11*x + B31), (B22*y + B32), z]
        
#        start_time = time.ticks_us()
        
        Pin(self.xmPin, mode = Pin.OUT_PP, value = 0)
        Pin(self.xpPin, mode = Pin.OUT_PP, value = 1)
        Pin(self.ypPin, mode = Pin.IN)
        ymADC = ADC(self.ymPin)
        xOutRaw = ((ymADC.read()/4096)*176)-88
           # may need to change the hard coded values
                                        # get from B matrix
                                        # x = (B11*x + B31)
                                        
        Pin(self.ypPin, mode = Pin.OUT_PP, value = 1)
        xpADC = ADC(self.xpPin)
        
        Pin(self.xpPin, mode = Pin.IN)
        Pin(self.ymPin, mode = Pin.OUT_PP, value = 0)
        xmADC = ADC(self.xmPin)
        
        yOutRaw = ((xmADC.read()/4096)*100)-50
        
        if xpADC.read() > 850: #795
            contact = 1
            xOut = self.B[0][0]*xOutRaw + self.B[2][0] 
            yOut = self.B[1][1]*yOutRaw + self.B[2][1]
        else:
            contact = 0
            yOut = 0
            xOut = 0
            
#        end_time = time.ticks_us()             # code for timing the scan
#        timedif = end_time - start_time
            
        # To recalibrate change to commented out return statement
        # and then calculate B matrix using MatrixMath.py
        # and then replace hard coded values in xOut and yOut
        
        #return xOutRaw, yOutRaw, contact
        return xOut, yOut, contact
        #return timedif

#
if __name__ == '__main__':

    ADcarry = RTpanel()
    ADcarry.UpdateBMatrix([[-1.03125, 67.58985, -72.31641, -71.97266, 67.28906], [-0.8300781, 29.73633, 30.41992, -33.54492, -31.71387], [1, 1, 1, 1, 1]])

    while True:
        
        print(ADcarry.xyzScanFast())