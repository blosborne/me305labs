'''!    @file main6.py
        @page labFF Term Project: Ball Balancer
        @brief                                  Runs the following tasks; user_task.py, encoder_task.py
        @details                                Initializes shares and then runs;
                                                    user_task.py
                                                    encoder_task.py
                                                    motor_task.py
        @brief All Term Project Files Can Be Seen <a href="https://bitbucket.org/blosborne/me305labs/src/master/Term%20Project/">Here</a>:
        @n
        @section sec_video User Demonstration
        @brief  The following video shows the user interface for the ball balancer. From this interface the user can check 
                if either the IMU or the touch panel need calibration, on startup the program checks to ensure calibration 
                is correct, if not it will prompt a message stating calibration is required and the user can press ‘L’ to 
                display calibration instructions for both the IMU and the touch panel. The program allows the user to set 
                both Kp and Kd gain values for both the inner and outer loop as well as enable closed-loop control, or 
                ball balance control. Features from Lab 5 have also been carried over including printing all Euler angles 
                and velocities as well as being able to set the platform to a specific angle if needed. The user can also 
                print ball positions, and collect data for 10 seconds. All processes have standard user error checks such 
                as not allowing the user to set the device to extreme gains or angles as well as ensuring the user can 
                exit any input stage by pressing enter without altering the previous values.  
        \htmlonly
        <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/689884737?h=74bf594904&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="IMG_0245"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>       
        \endhtmlonly
        @section sec_data Data Collection 
        @brief  We decided to focus on the user interface and data collection of the ball balancer more than tuning the 
                device to properly balance the ball. Our user interface allows the user to collect data during the 
                balancing of the ball for 10 seconds which is automatically written and stored onto the board through a 
                .txt file. Using a custom excel sheet the user can then import the .txt document via excel macros and 
                the data and graphs will be updated automatically. Plotted data includes position vs angle, velocity vs 
                angle, position vs time, and x position vs y position.
                
        @section sec_photos User Interface Photos
        Screen shots of the putty user interface can be seen below
        @image html mainMenu.PNG
        @brief Figure 1. Help menu for the user interface showing all possible inputs
        @image html calibrationInstructions.PNG
        @brief Figure 2. Instructions for showing the user how to calibrate both the platform and the touch panel,
               with specific inputs for starting the calibration process, note if the device is already calibrated 
               and the user tries to begin calibration a print statement will inform the user calibration is not required.
        @image html panelCalibration.PNG
        @brief Figure 3. A screenshot of the touch panel calibration in process, the stored coefficents are written to 
               a .txt file similar to the IMU calibration process. 
        @image html enteringGains.PNG
        @brief Figure 4. A screenshot of the user interface when entering gain values for both ball balancing and 
               closed loop control. The user can exit this interface by pressing enter assuming no values are entered.
        @image html collectingData.PNG
        @brief Figure 5. A screenshot of the user interface when data collection is occuring, during data collection
               all user inputs are allowed including changing the gain or disabling the ball balancer. Once collection
               has completed the data is written to a .txt file and can be imported to the custom excel document. 
        @image html excel.JPG
        @brief Figure 6. A screen shot of the custom excel file which allows the user to import the ball_balancer_data.txt
               file, the excel file automatically updates without needing to copy any data. This excel file can be found <a href="https://bitbucket.org/blosborne/me305labs/src/master/Term%20Project/Ball_Balance_Data.xlsm">here</a>:
        
        @section sec_diagrams Functionality Diagrams
        @image html TPTaskDiagram.JPG  
        @brief Figure 7. Task diagram for the ball balancer 
        @image html TPmotorFSM.jpg  
        @brief Figure 8. Motor task finite state diagram for the ball balancer                                      
        @image html TPpanFSM.jpg  
        @brief Figure 9. Touch panel task finite state diagram for the ball balancer 
        @image html TPBN0FSM.jpg  
        @brief Figure 10. BNO055 task finite state diagram for the ball balancer 
        @image html TPdataFSM.JPG  
        @brief Figure 11. Data task finite state diagram for the ball balancer
        @image html TPuserFSM.jpg  
        @brief Figure 12. User task finite state diagram for the ball balancer 
        @author                                 OConnell, Joseph
                                                Osborne, Barrett
        @date                                   March 18, 2022
'''

import user_task , shares, BNO055_task, motor_task, rtPanel_task, data_task

##  @brief      Flag to show that the user pressed 'z'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'z'
#               encoder_task.py will make this value false after the encoder is zeroed
#
zFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'p'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'p'
#               encoder_task.py will make this value false after the encoder position is acquired
#
pFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'd'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'd'
#               encoder_task.py will make this value false after the encoder delta is acquired
#
dFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'g'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 'g'
#               encoder_task.py will make this value false after the encoder data is acquired
#
gFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 's'
#   @details    Used for communication between user_task.py and encoder_task.py.
#               user_task.py will make this value true when the user presses 's'
#               encoder_task.py will make this value false after the encoder data collection is stopped
#
sFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'e'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'e'
#               used to tell motor_task.py to enable the motors
#
eFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'n'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'n'
#               Used to throw system intoa state inwhich it collects a duty cycle value from the user
#
nFlag = shares.Share(False)


##  @brief      Flag to show that the user pressed 'm'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'm'
#               Used to throw system intoa state inwhich it collects a duty cycle value from the user
#
mFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'c'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'c'
#               Used to tell motor_task.py to clear the faults that have occured in the motor
#
cFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'n'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'n'
#               Used to throw system into a state inwhich it collects a duty cycle value
#               from the user repeatedly
#
tFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'w'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'w'
#               Used to enable disable closed-loop control
#
wFlag = shares.Share(False)

##  @brief      Flag to show that the user pressed 'r'
#   @details    Used for communication between user_task.py and motor_task.py.
#               user_task.py will make this value true when the user presses 'r'
#               Used to perform a step response on motor 1
#
rFlag = shares.Share(False)

##  @brief      A shared variable used to store IMU position
#   @details    BNO055_task.py will give this variable a value and user_task.py will print this value
#
position1 = shares.Share((0,0,0))

##  @brief      A shared variable used to store encoder delta
#   @details    encoder_task.py will give this variable a value and user_task.py will print this value
#
delta = shares.Share(0)

##  @brief      A shared variable used to store duty cycle of motor 1
#
duty1 = shares.Share(0)

##  @brief      A shared variable used to store duty cycle of motor 2
#
duty2 = shares.Share(0)

##  @brief      A shared variable used to store the angular velocity of motor 1
#
vel = shares.Share(0)

##  @brief      A shared variable used to store the reference value for closed loop control
#
ref_roll = shares.Share(0)

##  @brief      A shared variable used to store the reference value for closed loop control
#
ref_pitch = shares.Share(0)

##  @brief      A shared variable used to store the Kp for closed loop control
#
Kp = shares.Share(0)

##  @brief      A shared variable used to store the Kd for closed loop control
#
Kd = shares.Share(0)

##  @brief      A shared variable used to store the calibration status of the BNO055
#
calStatReturn = shares.Share(0)

##  @brief      Used for communication between user_task.py and BNO055_task.py.
#
calFlag = shares.Share(False)

##  @brief      A shared variable used to store the calibration data for BNO055
#
calDATA = shares.Share(0)

##  @brief      A shared variable used to store ball position
#
ballXYZ = shares.Share((0,0,0))

##  @brief      A shared variable used to store ball position
#
Xtranspose = shares.Share([[-1.03125, 67.58985, -72.31641, -71.97266, 67.28906], [-0.8300781, 29.73633, 30.41992, -33.54492, -31.71387], [1, 1, 1, 1, 1]])

##  @brief      Used for communication between rtPanel_task.py and user_task.py.
#
bFlag = shares.Share(False)

##  @brief      A shared variable used to store the Kp for closed loop control
#
KpOut = shares.Share(0)

##  @brief      A shared variable used to store the Kd for closed loop control
#
KdOut = shares.Share(0)

##  @brief      A shared variable used to store the ball velocity
#
ballVel = shares.Share((0, 0))

##  @brief      A shared variable used to store the ball position
#
ballPos = shares.Share((0, 0))

task4 = user_task.taskUserFcn ("T1" , 10000, ref_pitch, ref_roll, Kd, calFlag, pFlag, eFlag, cFlag, position1, duty1, duty2, vel, calStatReturn, wFlag, Kp, zFlag, ballXYZ, bFlag, gFlag, KpOut, KdOut, Xtranspose, dFlag)
task3 = motor_task.taskMotorFcn("T3", 10000,  eFlag, nFlag, mFlag, cFlag, sFlag, tFlag, duty1, duty2, ref_roll, ref_pitch, Kp, Kd, wFlag, rFlag, vel, position1, ballXYZ, bFlag, KpOut, KdOut, Xtranspose, ballVel, ballPos)
task1 = BNO055_task.taskBNO055Fcn("T4", 10000, pFlag, position1, vel, calStatReturn, calFlag, eFlag, calDATA, cFlag)
task2 = rtPanel_task.taskRTpanelFcn("T2", 10000, zFlag, ballXYZ, calFlag, eFlag, calDATA, bFlag, Kp, Kd, position1, vel, KpOut, KdOut, ballVel, ballPos, Xtranspose)
task5 = data_task.taskDataCollection("T5", 10000, ballPos, ballVel, position1, dFlag)

tasklist = [task1, task3, task4, task2, task5]

if __name__  == '__main__':
    
    while True:
    
        try:
            for task in tasklist:
                next(task)
                
        except KeyboardInterrupt:
            break
    
    

    print("Program Terminating")
        
        
        

