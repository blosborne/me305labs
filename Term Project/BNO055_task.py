'''!@file BNO055_task.py
    @brief          Creates an BNO055 object (IMU)
    @details        Creates a BNO055 object and retrieves data from this IMU.
                    This task retrieves the Euler angles from the IMU
                    This task retrieves the angular velocity of the platform
                    This task retrieves the calibration status
                    
                    This task also reads and write to a calibration file 'IMU_cal_coeffs.txt'
    
    @author         O'Connell, Joseph
    @author         Osborne, Barrett
    @date           March 18, 2022
'''

import time, BNO055, os
from pyb import I2C 

def taskBNO055Fcn(taskName, period, pFlag, position1, vel, calStatReturn, calFlag, eFlag, calDATA, cFlag):
    '''!@brief          Serves as a medium between user_task.py and BNO055.py
        @details        Facillitates communication between user_task.py and the IMU driver,
                        BNO055.py. This code will recieve signals from user_task and go and get
                        information from the IMU driver.
                            
        @param taskName         Name of task
        @param period           Period in which this task operates (time)
        @param pFlag            Tells this task in 'p' has been pressed
        @param position1        IMU euler angles
        @param vel              IMU angular velocity
        @param calStatReturn    IMU calibration status
        @param calFlag          Tells this task that calibration needs to occur
        @param eFlag            Tells this task in 'e' has been pressed
        @param calDATA          calibration data
        @param cFlag            Tells this task in 'c' has been pressed
    '''

    ##  @brief      Keeps track of which state the BNO055 task is in
    #   
    state = 0
    
    ##  @brief      Calibration file name
    # 
    filename = "IMU_cal_coeffs.txt"
    
    ##  @brief      next time that task will need to move on
    #   
    next_time = time.ticks_add(time.ticks_us() , period)
    
    ##  @brief      I2C object
    # 
    my_i2c = BNO055.BNO055(I2C(1, I2C.CONTROLLER))

    while True:
            
            ##  @brief      Current time of the system
            #
            current_time = time.ticks_us()
            position1.write(my_i2c.eulerAng())
            calStatReturn.write(my_i2c.calStat())
            vel.write(my_i2c.angVel())       
            
            if time.ticks_diff(current_time, next_time) >=0:
                next_time = time.ticks_add(next_time, period)
                
                if state == 0:
                    if filename in os.listdir():
                        state = 2
                    else:
                        cFlag.write(True)
                        state = 3
                    
                elif state == 1:    
                    position1.write(my_i2c.eulerAng())                    
                    
                    if pFlag.read() == True:
                        position1.write(my_i2c.eulerAng())
                        pFlag.write(False)
                    
                    elif calFlag.read() == True:
                        calStatReturn.write(my_i2c.calStat())
                        print(calStatReturn.read())
                        calFlag.write(False)
                        
                    elif eFlag.read() == True:
                        my_i2c.configMode()
                        my_i2c.imuMode()
                        eFlag.write(False)
                        position1.write(my_i2c.eulerAng())
                        
                elif state == 2: #read state                 
                    with open(filename, 'r') as f:
                        calDataString = f.readline()
                        
                    calCoeffs = calDataString.split(',')
                    
                    for i in range(0, len(calCoeffs)):
                        calCoeffs[i] = int(calCoeffs[i], 16)
                        my_i2c.bufcal[i] = calCoeffs[i]
                        
                    my_i2c.configMode()
                    my_i2c.calCoefWrite(my_i2c.bufcal)
                    my_i2c.imuMode()
                    state = 1
                    
                elif state == 3: #write state
                    my_i2c.imuMode
                    my_i2c.calStat()
                    calDATA.write((my_i2c.mag_stat, my_i2c.acc_stat, my_i2c.gyr_stat, my_i2c.sys_stat))
                    yield calDATA
                    if my_i2c.mag_stat == 0 and my_i2c.acc_stat == 3 and my_i2c.gyr_stat == 3 and my_i2c.sys_stat == 0:
                        my_i2c.calCoefRead()
                        calCoeffs = my_i2c.calCoefRead()
                        strList = []
                        for calCoeff in calCoeffs:
                            strList.append(hex(calCoeff))
                        with open(filename, 'w') as f:
                            f.write(','.join(strList))
                            state = 1
                            
                            
                        
                    
                else:
                    raise ValueError(f"invalid state in {taskName}")
                
                next_time = time.ticks_add(next_time , period)
                
                yield state
                
            else:
                yield None