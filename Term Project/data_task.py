'''!@file Data_task.py
    @brief          Collects data from ball balancing platform
    @details        Takes in shared values for ball position, ball velcoity, and platform angle.
                    The ball position and velocity are taken from rtPanel_task.py
                    The platform angle is taken from BNO055_task.py
                    
    @author         O'Connell, Joseph
    @author         Osborne, Barrett
    @date           March 18, 2022

'''

import time, array

def taskDataCollection(taskName, period, ballPos, ballVel, position1, dFlag):
    '''!@brief          This task is in charge of interfacing with the motors
        @details        Runs controls 
                            
        @param taskName         Name of task
        @param period           Period in which this task operates (time)
        @param ballPos          Stores X and Y posiitons of the ball
        @param ballVel          Stores ball velocities
        @param position1        IMU euler angles
        @param dFlag            Tells this task in 'd' has been pressed
    '''
    
    ##  @brief      Keeps track of which state the data task is in
    #
    state = 0
    
    ##  @brief      Ball balance data
    # 
    filename = "Ball_Balance_Data.txt"
    
    ##  @brief      next time that task will need to move on
    #   
    next_time = time.ticks_add(time.ticks_us() , period)

    while True:
            
            current_time = time.ticks_us()
            
            if time.ticks_diff(current_time, next_time) >=0:
                next_time = time.ticks_add(next_time, period)
                
                if state == 0:
                
                    # Ball position arrays
                    xData = array.array( 'f' , 500*[0])
                    yData = array.array( 'f' , 500*[0])
                    
                    # Ball velocity arrays
                    xVelData = array.array( 'f' , 500*[0])
                    yVelData = array.array( 'f' , 500*[0])
                    
                    # Platform angle arrays
                    pitchData = array.array( 'f' , 500*[0])
                    rollData = array.array( 'f' , 500*[0])
                    
                    state = 1
                    
                elif state == 1: 
                    
                    if dFlag.read():
                        print("Collecting Ball Balance Data")
                        index = 0
                        dFlag.write(False)
                        state = 2
                        
                elif state == 2: 
                    xData[index], yData[index] = ballPos.read()
                    
                    xVelData[index], yVelData[index] = ballVel.read()
                    
                    rollData[index], pitchData[index], headAngle = position1.read()
                         
                    index = index + 1
                    
                    if index == 500:
                        state = 3
                        
                elif state == 3:
                    print("Done Collecting Data")
                    with open(filename, 'w') as f:
                        for (item1, item2, item3, item4, item5, item6) in zip(xData, yData, xVelData, yVelData, pitchData, rollData):
                            f.write(f"{item1:.2f}, {item2:.2f}, {item3:.2f}, {item4:.2f}, {item5:.2f}, {item6:.2f} \r\n")
                        state = 1
                        yield None
                        
                    
                else:
                    raise ValueError(f"invalid state in {taskName}")
                
                next_time = time.ticks_add(next_time , period)
                
                yield state
                
            else:
                yield None
        